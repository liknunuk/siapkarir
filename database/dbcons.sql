-- DROP TABLE IF EXISTS xxxx;
-- CREATE TABLE xxxx();

-- INFORMASI SISWA --

-- tabel identitas siswa
DROP TABLE IF EXISTS siswa;
CREATE TABLE siswa(
  nis varchar(12) NOT NULL,
  nama varchar(40) NULL,
  jnsKelamin enum('Laki-laki','Perempuan') DEFAULT 'Laki-laki',
  tempatLahir varchar(30) DEFAULT 'Banjarnegara',
  tanggalLahir date,
  jurusan enum('IPA','IPS','Tata Busana','Agama') DEFAULT 'Agama',
  rencanaKarir enum('Kuliah','Kerja','Kursus','Wirausaha','Mondok') DEFAULT 'Kuliah',
  kepribadian text,
  aktifitas enum('aktif','non aktif') default 'aktif',
  PRIMARY KEY (nis)
);

-- tabel nilai masuk
DROP TABLE IF EXISTS nilaiSMP;
CREATE TABLE nilaiSMP(
  nis varchar(12) NOT NULL,
  abp float(6,2) default 0.00,    -- Pend. Agama dan Budi Pekerti
  pkn float(6,2) default 0.00,    -- Pend. Kewarganegaraan
  ind float(6,2) default 0.00,    -- Bhs. Indonesia
  mtk float(6,2) default 0.00,    -- Matematika
  ipa float(6,2) default 0.00,    -- Ilmu Peng. Alam
  ips float(6,2) default 0.00,    -- Ilmu Peng. Sosial
  ing float(6,2) default 0.00,    -- Bhs. Inggris
  sbk float(6,2) default 0.00,    -- Seni Budaya   
  pjk float(6,2) default 0.00,    -- Pendidikan Jasmani
  mlk float(6,2) default 0.00,    -- Muatan Lokal
  -- mapel mts
  aqh float(6,2) default 0.00,     -- Alquran dan Hadtits
  aqa float(6,2) default 0.00,     -- Aqidah dan Akhlak 
  fqh float(6,2) default 0.00,     -- Fiqih
  ski float(6,2) default 0.00,     -- Sejarah Kebudayaan Islam
  arb float(6,2) default 0.00,     -- Bahasa Arab
  
  PRIMARY KEY (nis)
);


-- tabel kamus data
DROP TABLE IF EXISTS kamus;
CREATE TABLE kamus(
  idx int(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  klp varchar(10) NULL,
  kode varchar(20) NULL,
  arti text,
  PRIMARY KEY (idx)
);

-- DUMP data kamus
INSERT INTO kamus (klp,kode,arti) VALUES
('mpsmp','abp','Pend. Agama dan Budi Pekerti'),
('mpsmp','pkn','Pend. Kewarganegaraan'),
('mpsmp','ind','Bahasa Indonesia'),
('mpsmp','mtk','Matematika'),
('mpsmp','ipa','Ilmu Peng. Alam'),
('mpsmp','ips','Ilmu Peng. Sosial'),
('mpsmp','ing','Bahasa Inggris'),
('mpsmp','sbk','Seni Budaya'),
('mpsmp','pjk','Pendidikan Jasmani, Olah Raga dan Kesehatan'),
('mpsmp','aqh','Alquran dan Hadits'),
('mpsmp','aqa','Aqidah dan Akhlak'),
('mpsmp','fqh','Fiqih'),
('mpsmp','arb','Bahasa Arab'),
('mpsmp','ski','Sejarah Kebudayaan Islam'),
('mpsmp','mlk','Muatan Lokal'),
('mpmaa','a_aqhd','AlQuran dan Hadits'),
('mpmaa','a_aqkh','Aqidah dan Akhlaq'),
('mpmaa','a_fiqh','Fiqh'),
('mpmaa','a_skbi','Sejarah Kebudayaan Islam'),
('mpmaa','a_ppkn','Pendidikan Pancasila dan Kewarganegaraan'),
('mpmaa','a_bind','Bahasa Indonesia'),
('mpmaa','a_barb','Bahasa Arab'),
('mpmaa','a_mttk','Matematika'),
('mpmaa','a_sjid','Sejarah Indonesia'),
('mpmaa','a_bing','Bahasa Inggris'),
('mpmaa','b_sbdy','Seni Budaya'),
('mpmaa','b_pjok','Pend. Jasmani Olahraga dan Kesehatan'),
('mpmaa','b_pkwu','Prakarya dan Kewirausahaan'),
('mpmaa','ca_mtk','Matematika'),
('mpmaa','ca_bio','Biologi'),
('mpmaa','ca_fsk','Fisika'),
('mpmaa','ca_kim','Kimia'),
('mpmaa','cs_geo','Geografi'),
('mpmaa','cs_sej','Sejarah'),
('mpmaa','cs_sos','Sosiologi'),
('mpmaa','cs_eko','Ekonomi'),
('mpmaa','cb_sid','Bahasa dan Sastra Indonesia'),
('mpmaa','cb_sig','Bahasa dan Sastra Inggris'),
('mpmaa','cb_sal','Bahasa dan Sastra Asing Lainnya'),
('mpmaa','cb_atp','Antropologi'),
('mpmaa','cg_taf','Ilmu Tafsir'),
('mpmaa','cg_hds','Ilmu Hadits'),
('mpmaa','cg_fqh','Ilmu Fiqh'),
('mpmaa','cg_klm','Ilmu Kalam'),
('mpmaa','cg_akh','Akhlaq'),
('mpmaa','cg_arb','Bahasa Arab');

-- tabel nilai Aliyah Kelompok A dan B
DROP TABLE IF EXISTS nilaiMan;
CREATE TABLE nilaiMan(
  kodeNilai varchar(14) NOT NULL,    -- nis_smt xxxxxxxxxxx_x
  a_aqhd varchar(7) default '000/000',
  a_aqkh varchar(7) default '000/000',
  a_fiqh varchar(7) default '000/000',
  a_skbi varchar(7) default '000/000',
  a_ppkn varchar(7) default '000/000',
  a_bind varchar(7) default '000/000',
  a_barb varchar(7) default '000/000',
  a_mttk varchar(7) default '000/000',
  a_sjid varchar(7) default '000/000',
  a_bing varchar(7) default '000/000',
  b_sbdy varchar(7) default '000/000',
  b_pjok varchar(7) default '000/000',
  b_pkwu varchar(7) default '000/000',
  ca_bio varchar(7) default '000/000',
  ca_mtk varchar(7) default '000/000',
  ca_fsk varchar(7) default '000/000',
  ca_kim varchar(7) default '000/000',
  cs_geo varchar(7) default '000/000',
  cs_sej varchar(7) default '000/000',
  cs_sos varchar(7) default '000/000',
  cs_eko varchar(7) default '000/000',
  cb_sid varchar(7) default '000/000',
  cb_sig varchar(7) default '000/000',
  cb_sal varchar(7) default '000/000',
  cb_atp varchar(7) default '000/000',
  cg_taf varchar(7) default '000/000',
  cg_hds varchar(7) default '000/000',
  cg_fqh varchar(7) default '000/000',
  cg_klm varchar(7) default '000/000',
  cg_akh varchar(7) default '000/000',
  cg_arb varchar(7) default '000/000',
  PRIMARY KEY (kodeNilai)
);

-- tabel skor test bakat minat / iq
-- DROP TABLE IF EXISTS bakatMinat;
-- CREATE TABLE bakatMinat(
--   nis varchar(12) NOT NULL,
--   xxxxx int(3) default 0,
--   PRIMARY KEY (nis)
-- );

-- tabel skor AKPD - Angket Kebutuhan Peserta Didik
-- DROP TABLE IF EXISTS akpd;
-- CREATE TABLE akpd(
--   nis varchar(12) NOT NULL,
--   xxxxx text,
--   PRIMARY KEY(nis)
-- );

-- tabel rekomendasi
DROP TABLE IF EXISTS rekomendasi;
CREATE TABLE rekomendasi(
  nis varchar(12) NOT NULL,
  orangTua text,
  teman text,
  saudara text,
  kolega text,
  gurubk text,
  PRIMARY KEY(nis)
);

-- INFORMASI STAKE HOLDER

-- tabel stakeholder
DROP TABLE IF EXISTS stakeHolder;
CREATE TABLE stakeHolder(
  idsh int(3) UNSIGNED AUTO_INCREMENT,
  nama varchar(50) NOT NULL,
  kategori enum('Kuliah','Kerja','Kursus','Wirausaha','Mondok') DEFAULT 'Kuliah',
  contactPerson varchar(30),
  alamat tinytext,
  telepon tinytext,
  email tinytext,
  website tinytext,
  PRIMARY KEY(idsh)
);

-- INFORMASI TRANSAKSI siswa-stakeholder
-- tabel Berita
DROP TABLE IF EXISTS berita;
CREATE TABLE berita(
  idBerita int(6) UNSIGNED AUTO_INCREMENT,
  rilis date,
  author int(3) UNSIGNED COMMENT 'ID Stake Holder',
  judul tinytext,
  konten text,
  tags tinytext,
  PRIMARY KEY(idBerita)
);

-- tabel agenda
DROP TABLE IF EXISTS agenda;
CREATE TABLE agenda(
  idAgenda int(6) UNSIGNED AUTO_INCREMENT,
  promotor int(3) UNSIGNED COMMENT 'ID Stake Holder',
  namaAgenda tinytext,
  waktu datetime,
  tempat varchar(50),
  deskripsi text,
  siswaHadir text,
  PRIMARY KEY(idAgenda)
);

-- tabel
-- DROP TABLE IF EXISTS presensiAgenda;
-- CREATE TABLE presensiAgenda(
--   idxPresensi int(6) UNSIGNED AUTO_INCREMENT,
--   idxAgenda int(6) UNSIGNED,
--   siswaHadir text COMMENT 'deretan NIS',
--   PRIMARY KEY(idxPresensi)
-- );


