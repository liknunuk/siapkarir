<div class="container">
    <div class="row">
        <div class="col-lg-12 homeJudul">
            <h1>KULIAH</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <article>
            <p>
            Kuliah adalah belajar, mengikuti pelajaran di perguruan tinggi. Dalam kuliah bisa untuk meningkatkan komitmen dan kesungguhan serta melatih pola pikir, analisa dan disiplin diri sebagai seorang intelektual. Menuntut ilmu, menguasai bidang tertentu. Perbedaan Antara Perguruan Tinggi Negeri (PTN) , Perguruan Tinggi Swasta (PTS) dan Perguruan Tinggi Kedinasan (PTK)
            </p>

            <p class="subtitle">
                Perguruan Tinggi Negeri (PTN)
            </p>
            <p>
            Perguruan Tinggi Negeri (PTN) adalah perguruan tinggi yang di kelola oleh pemerintahan baik di bawah Departemen Pendidikan Nasional maupun di bawah departemen lain milik pemerintah. Contoh perguruan tinggi negeri yang terdapat di Indonesia, yaitu : Universitas Indonesia (UI), Universitas Gajah Mada (UGM), Institut Teknologi Bandung (ITB), Universitas Brawijaya (UB), Universitas Airlangga (UNAIR), Institut Pertanian Bogor (IPB) dan Universitas Negeri Jakarta (UNJ)
            </p>

            <p class="subtitle">
            Perguruan Tinggi Swasta (PTS)
            </p>
            <p>
            Perguruan Tinggi Swasta (PTS) adalah perguruan tinggi yang dimilliki dan dikelola oleh perorangan atau kelompok/yayasan tertentu. Umumnya, perguruan tinggi negeri (PTN) mendapat subsidi dari pemerintah dalam pengelolaan dan pelaksaan  pendidikan. Berbeda dengan perguruan tinggi swasta (PTS), pembiayaan pengelolaan dan pelaksaan pendidikan menjadi tanggung jawab perguruan tinggi yang bersangkutan sepenuhnya. Pemerintah hanya bertugas sebagai pengawas dan pemberi ketentuan kurikurum dalam proses pembelajaran dengan undang-undang yang berlaku. Pemerintah mengawasi dengan adanya lembaga nabimbingan dan pengawasan atas penyelenggaraan perguruan tinggi swasta (PTS) yang pada mulanya bernama  Lembaga Perguruan Tinggi Swasta (LPTS) dan kemudian di ubah menjadi Koordinasi Perguruan Tinggi Swasta (KOPERTiSI). Contoh perguruan tinggi swasta yang terdapat di Indonesia, yaitu : Universitas Katolik Parahyangan (UNPAR), Universitas Tarumanegara (UNTAR), Universitas Pasundan (UNPAS), Universitas Pelita Harapan (UPH), Universitas Islam Indonesia (UII), Universitas Muhammadiah Malanng (UMM) dan Institut Teknologi Nasional (ITENAS)
            </p>

            
            <p class="subtitle">
            Perguruan Tinggi Kedinasan (PTK)
            </p>
            <p>
            Perguruan Tinggi Kedinasan (PTK) adalah perguruan tinggi di bawah departemen  selain Departemen Pendidikan Nasional atau merupakan lembaga pendidikan tinggi negeri yang memiliki ikatan dengan lembaga pemerintahan sebagai penyelenggara pendidikan. Perguruan Tinggi Kedinasan (PTK) itu sendiri terdapat 2 macam, yaitu:
            </p>
            <ul>
                <li>
                <p class="subtitle1">
                Ikatan Dinas
                </p>
                <p>
                Perguruan Tinggi Kedinasan (PTK) dimana seluruh biaya kuliah ditanggung oleh pemerintah (gratis), prospek ke depan jelas (dapat diangkat jadi CPNS) dan terdapat pemberian uang saku untuk mahasiwa atau istilahnya tunjangan ikatan dinas.
                </p>
                </li>
                <li>
                <p class="subtitle1">
                Kedinasan
                </p>
                <p>
                Perguruan Tinggi Kedinasan (PTK) yang berada dibawah naungan departemen, namun tidak ikatan dinas, artinya mahasiswanya tetap dipungut biaya kuliah,  setelah lulus kuliah bisa jadi PNS dan bisa tidak, dan tidak dapat uang saku.<br/>
                Contoh perguruan tinggi kedinasan yang terdapat di Indonesia, yaitu; Sekolah Tinggi Akuntansi Negara (STAN), Akademi Imigrasi (AIM), Sekolah Tinggi Ilmu Statistik (STIS), Akademi Kimia Analisis (AKA), (Sekolah Tinggi Teknlogi Tekstil (STTT) dan Akademi Meteorogi dan Geofisika (AMG).
                </p>
                </li>
            
            </ul>
            </article>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
