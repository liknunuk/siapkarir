<div class="container">
  <div class="row">
    <div class="col-lg-12 homeJudul">
      <h1>JUDUL</h1>
    </div>
  </div>
    <div class="row homeKonten">
        <div class="col-lg-12">
            <article>
            <p>
            Bekerja adalah sesuatu yang dilakukan untuk mencari nafkah; mata pencaharian. Pekerjaan yang sesuai dengan minat & tipe kepribadian adalah idaman setiap orang. Apabila kita bekerja di bidang yang sesuai dengan minat dan tipe kepribadian,umumnya akan lebih sukses dalam menjalani karir, karena pekerjaan terasa lebih menyenangkan. <br/>
            Tidaklah mudah bagi kita untuk menemukan pekerjaan idaman yang sesuai dengan minat dan kepribadian kita. Apabila kita bekerja di bidang yang sesuai dengan minat dan tipe kepribadian, pada umumnya lebih sukses dalam menjalani karir. Kesesuaian itulah yang membuat orang lebih mencintai dan bahagia dalam menjalankan pekerjaannya, dampaknya pun kita bisa bekerja lebih giat dan rasa tanggung jawab pun semakin tinggi. Untuk itu, marilah kita bahas potensi, minat dan kepribadian Siswa diharapkan bisa mempermudah dalam memilih jenis pekerjaan yang sesuai.
            </p>
            </article>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
