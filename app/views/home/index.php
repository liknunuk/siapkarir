<div class="container-fluid">
    <div class="row d-flex justify-content-center mt-3" id="carousel-block">
        <div class="col-lg-8">
            <div id="frontCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="<?=BASEURL;?>img/kuliah.png" alt="Kuliah">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Berprestasi di Perguruan Tinggi</h5>
                            <p>Bimbingan menuju sukses di perguruan tinggi pilihan</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?=BASEURL;?>img/pengusaha.png" alt="Pengusaha">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Wirausaha Sukses Mandiri</h5>
                            <p>Bimbingan merintis usaha sampai mampu mengelola secara mandiri</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?=BASEURL;?>img/santri.png" alt="Santri">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Menguasai Ilmu Dunia Akhirat</h5>
                            <p>Bimbingan menjemput kemudahan menempuh pendidikan di pondok pesantren</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?=BASEURL;?>img/kursus.png" alt="Kursus">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Menambah Wawasan dan Keterampilan</h5>
                            <p>Panduan menemukan tempat belajar vokasi sesuai bidang yang di minati</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="<?=BASEURL;?>img/karyawan.png" alt="Karyawan">
                        <div class="carousel-caption d-none d-md-block">
                            <h5>Menempuh Karir Profesional</h5>
                            <p>Panduan mendapatkan bidang pekerjaan yang layak dan terhormat</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">

        <div class="card col-sm-4">                    
            <div class="card-body">
                <h4 class="card-title">Info Perguruan Tinggi</h4>
                <p class='stakeHolder'>Kuliah adalah belajar, mengikuti pelajaran di perguruan tinggi. Dalam kuliah bisa untuk meningkatkan komitmen dan kesungguhan serta melatih pola pikir, analisa dan disiplin diri sebagai seorang intelektual. Menuntut ilmu, menguasai bidang tertentu. Perbedaan Antara Perguruan Tinggi Negeri (PTN) , Perguruan Tinggi Swasta (PTS) dan Perguruan Tinggi Kedinasan (PTK)</p>
                <a href="<?=BASEURL;?>Home/Kuliah">Baca lebih lanjut</a>
            </div>
        </div>            
                
        <div class="card col-sm-4">                    
            <div class="card-body">
                <h4 class="card-title">Info Wirausaha</h4>
                <p class='stakeHolder'>
                Wirausaha adalah sebuah kegiatan usaha atau suatu bisnis mandiri yang setiap sumber daya dan kegiatannya dibebankan kepada pelaku usaha atau wirausahawan terutama dalam hal membuat produk baru, menentukan bagaimana cara produksi baru, maupun menyusun suatu operasi bisnis dan pemasaran produk serta mengatur permodalan usaha. Wirausaha memiliki tujuan untuk menghasilkan sesuatu yang memiliki nilai lebih tinggi dibandingkan sebelum diolah. 
                </p>
                <a href="<?=BASEURL;?>Home/wirausaha">Baca lebih lanjut</a>
            </div>                    
        </div>            
                
        <div class="card col-sm-4">                    
            <div class="card-body">
                <h4 class="card-title">Info Pondok Pesantren</h4>
                <p class='stakeHolder'>
                Pondok pesantren merupakan tempat untuk menuntut ilmu yang memberikan banyak manfaat setelah kalian jadi alumni suatu lembaga pondok pesantren. Pondok pesantren tidak hanya mengajarkan kecerdasan intelektual, tapi juga ilmu agama yang bermanfaat untuk kehidupan mendatang.
                </p>
                <a href="<?=BASEURL;?>Home/pesantren">Baca lebih lanjut</a>
            </div>                    
        </div>            
        
        <div class="card col-sm-4">                    
            <div class="card-body">
                <h4 class="card-title">Pelatian Keterampilan</h4>
                <p class='stakeHolder'>
                Kursus adalah lembaga pelatihan yang termasuk ke dalam jenis pendidikan non formal. Kursus merupakan suatu kegiatan belajar-mengajar seperti halnya sekolah. Perbedaannya adalah bahwa kursus biasanya diselenggarakan dalam waktu pendek dan hanya untuk mempelajari satu keterampilan tertentu
                </p>
                <a href="<?=BASEURL;?>Home/kursus">Baca lebih lanjut</a>
            </div>                    
        </div>            
        
        <div class="card col-sm-4">                    
            <div class="card-body">
                <h4 class="card-title">Kerja</h4>
                <p class='stakeHolder'>Bekerja adalah sesuatu yang dilakukan untuk mencari nafkah; mata pencaharian. Pekerjaan yang sesuai dengan minat & tipe kepribadian adalah idaman setiap orang. Apabila kita bekerja di bidang yang sesuai dengan minat dan tipe kepribadian,umumnya akan lebih sukses dalam menjalani karir, karena pekerjaan terasa lebih menyenangkan</p>
                <a href="<?=BASEURL;?>Home/kerja">Baca lebih lanjut</a>
            </div>                    
        </div>            
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
$('.carousel').carousel({
  interval: 2000
})
</script>