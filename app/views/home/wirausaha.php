<div class="container">
  <div class="row">
    <div class="col-lg-12 homeJudul">
      <h1>JUDUL</h1>
    </div>
  </div>
  <div class="row homeKonten">
    <div class="col-lg-12">
      <article>
        <p>
        Wirausaha adalah sebuah kegiatan usaha atau suatu bisnis mandiri yang setiap sumber daya dan kegiatannya dibebankan kepada pelaku usaha atau wirausahawan terutama dalam hal membuat produk baru, menentukan bagaimana cara produksi baru, maupun menyusun suatu operasi bisnis dan pemasaran produk serta mengatur permodalan usaha. Wirausaha memiliki tujuan untuk menghasilkan sesuatu yang memiliki nilai lebih tinggi dibandingkan sebelum diolah. Menurut Burgess (1993), wirausaha adalah seseorang yang melakukan pengelolaan, mengorganisasikan, dan berani menanggung segala risiko dalam menciptakan peluang usaha dan usaha yang baru . Sedangkan  menurut J.B Say (1803), Wirausaha adalah pengusaha yang mampu mengelola sumber-sumber daya yang dimiliki secara ekonomis (efektif dan efisien) dan tingkat produktivitas yang rendah menjadi tinggi.<br/>
        Wirausaha juga memiliki ciri-ciri dan mungkin belum banyak yang mengertahuinya. Ciri-Ciri tersebut diantaranya:<br/>
        Memiliki keberanian dalam mengambil keputusan serta risiko,mempunyai daya kreasi serta inovasi tinggi, dapat berfikir jangka panjang untuk masa depan,memiliki jiwa kepemimpinan,memiliki semangat serta kemauan yang keras,dapat menganalisis dengan tepat.Memiliki sifat hemat atau tidak konsumtif.<br/>
        Para pelaku wirausaha disebut dengan wirausahawan. Setiap jenis usaha baik itu UKM, UMKM, maupun korporasi memiliki wirausahawan dibaliknya. Untuk membangun usaha tersebut tentunya sikap dan perilaku wirausaha disesuaikan dan harus bekerja keras agar dapat membuat bisnis yang lancar serta mampu membuat produk yang bisa diterima oleh target pasar.
        </p>
      </article>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
