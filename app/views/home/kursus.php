<div class="container">
  <div class="row">
    <div class="col-lg-12 homeJudul">
      <h1>JUDUL</h1>
    </div>
  </div>
  <div class="row homeKonten">
    <div class="col-lg-12">
      <article>
        <p>
        Kursus adalah lembaga pelatihan yang termasuk ke dalam jenis pendidikan non formal. Kursus merupakan suatu kegiatan belajar-mengajar seperti halnya sekolah. Perbedaannya adalah bahwa kursus biasanya diselenggarakan dalam waktu pendek dan hanya untuk mempelajari satu keterampilan tertentu. Peserta Kursus yang telah mengikuti kursus dengan baik dapat memperoleh sertifikat atau surat keterangan. Untuk keterampilan tertentu seperti, kursus ahli kecantikan atau penata rambut diwajibkan menempuh ujian negara. Ujian negara ini dimaksudkan untuk mengawasi mutu kursus yang bersangkutan, sehingga pelajaran yang diberikan memenuhi syarat dan peserta memiliki keterampilan dalam bidangnya.<br/>

        Berikut daftar Jenis Kursus yang terdaftar di Direktorat Pembinaan Kursus dan Pelatihan, Kementrian Pendidikan dan Kebudayaan Republik Indonesia:
        </p>
        <ul>
            <li><p>Kursus Komputer</p></li>
            <li><p>Kursus Bahasa Inggris</p></li>
            <li><p>Kursus Menjahit</p></li>
            <li><p>Kursus Tata Kecantikan Rambut</p></li>
            <li><p>Kursus Tata Rias Pengantin</p></li>
            <li><p>Kursus Tata Kecantikan Kulit</p></li>
            <li><p>Kursus Otomotif</p></li>
            <li><p>Kursus Mengemudi</p></li>
            <li><p>Kursus Tata Boga</p></li>
            <li><p>Kursus Seni Musik</p></li>
            <li><p>Kursus Mental Aritmatika</p></li>
            <li><p>Kursus Bordir</p></li>
            <li><p>Kursus Akuntansi</p></li>
            <li><p>Kursus Hantaran</p></li>
            <li><p>Kursus Bahasa Mandarin dan lain lain</p></li>
        </ul>
      </article>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
