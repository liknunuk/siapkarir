<div class="container">
  <div class="row">
    <div class="col-lg-12 homeJudul">
      <h1>JUDUL</h1>
    </div>
  </div>
  <div class="row homeKonten">
    <div class="col-lg-12">
      <article>
        <p>
        Mondok berasal dari kata pondok yang dari kata benda menjadi kata kerja dengan arti 'tinggal di pondok'. Mondok artinya tinggal di asrama atau bilik yang disediakan oleh pesantren, yang di dalamnya harus mengikuti kegiatan belajar di madrasah dan juga kegiatan di luar jam sekolah, yakni kegiatan pesantren.
        </p>
        <p>
        Pondok pesantren merupakan tempat untuk menuntut ilmu yang memberikan banyak manfaat setelah kalian jadi alumni suatu lembaga pondok pesantren. Pondok pesantren tidak hanya mengajarkan kecerdasan intelektual, tapi juga ilmu agama yang bermanfaat untuk kehidupan mendatang.
        </p>
        <p>
        Berbeda dengan sekolah umum, siswa di pondok pesantren tidak hanya diajarkan untuk mengejar kecerdasan intelektual tapi di Pondok pesantren juga menekankan kematangan emosional dan spiritual (EQ & SQ). Selain matang secara logika, siswa di pondok pesantren juga memiliki kecerdasan mengelola emosi dan kebutuhan batin.
        </p>
        <p>
            Berikut Ulasan  Kelebihan dan manfaat anak jika mondok.
        </p>
        <ul>
            <li>
                <p class="subtitle1">
                Anak Bakal Mandiri
                </p>
                <p>
                Jika anak mondok dan berstatus sebagai santri dia akan mempunyai pola pikir yang berbeda dan memiliki batas nafsu yang berlebihan, Sebab mereka terbiasa ngapa ngpaain sendiri mulai dari makan masak sendiri, nyuci baju, nyapu ngepel, bersihin kamar mandi, nyuci piring dll. Bahagianya Orang tua zaman Now bila anak mondok pergi ke Pesantren mereka akan memiliki standart akhlak mulia.
                </p>
            </li>
            
            <li>
                <p class="subtitle1">
                Skil Sosial Meningkat
                </p>
                <p>
                Di pesantren kebanyakan  santriwan dan santriwati nya berasal dari berbagai kalangan dan daerah ,Sehingga anak pesantren haru bisa bersosialisasi sama semua orang dengan berbagai latar belakang dan buadaya yang berbeda beda. Dengan adanya perbedaan tersebut tentu akan membuat  skil sosialisasi anak bakal meningkat.
                </p>
            </li>

            <li>
                <p class="subtitle1">
                Banyak Teman Yang Seperti Saudara Sendiri
                </p>
                <p>
                Kelebihan dan manfaat jika anak nyantri bakal mempunyai banyak teman karena di pondok banyak orang perantauan dari berbgai macam kota daerah yang tersebar. Setiap hari hidup berdampingan dan bersama-sama tentu membuat satu sama lain seperti saudara sendiri dengan adanya kegiatan atau saling gotong royong.
                </p>
            </li>

            <li>
                <p class="subtitle1">
                Kuat Fisik dan Mental
                </p>
                <p>
                Di pondok hampir semuanya diatur, dan jiaka ada yang melanggar sudah pasti otomatis mendapat hukuman, mulai dari di bentak bentak dan lain lain, dengan demikian mereka yang di pesantren lama kelamaan  akan terbentuk fisik dan mental yang kuat.
                </p>
            </li>

            <li>
                <p class="subtitle1">
                5.Terjaga dari pengaruh buruk
                </p>
                <p>
                Kebebasan  Anak yang tinggal di asrama pondok pesantren lingkungan belajarnya dibatasi dan ada skat-skat tertentu. Terpisahnya antara komplek putra dan putri, juga merupakan salah satu bukti bahwa lingkungan pondok pesantren itu
                terjaga dari pengaruh yang dapat merusak pola fikir santri.
                </p>
            </li>
            
            <li>
                <p class="subtitle1">
                Ibadah terjaga orang tua bahagia
                </p>
                <p>
                Yang terpenting lagi hidup di pondok pesantren adalah shalat lima waktu dan ibadah sunnah lainnya dapat terjaga berjalan dan terprogram dengan baik. Dengan menitipkan anak kita di pesantren anak akan dibina dan di didik ilmu agama secara bertahap sehingga mampu menguasai ilmu tersebut dengan baik. Sehingga bahagianya pasti orang tua di hari akhir nanti.
                </p>
            </li>
            
            <li>
                <p class="subtitle1">
                Pondok pesantren menanamkan budaya menghormati guru.
                </p>
                <p>
                Meskipun semua institusi pendidikan mengajarkan hal serupa, namun pondok pesantren membawanya ke level yang lebih jauh. Guru benar-benar dianggap sebagai orang yang menyampaikan ilmu. Tanpa guru, kehidupan manusia akan tersesat. Dan hanya di pondok pesantren, seorang benar-benar dihormati.
                </p>
            </li>
            
            <li>
                <p class="subtitle1">
                Belajar di pondok pesantren membuat anak-anak lebih terfokus untuk belajar.
                </p>
                <p>
                Hal ini karena budaya dalam pondok pesantren yang menjauhi kehidupan duniawi. Alhasil kabar soal tren terbaru sangat jarang sampai di telinga para penghuni pondok. Berita yang mereka juga lebih tersaring. Jadi hanya berita yang memang diperlukan yang akan disampaikan. Karena itu, mereka menjadi terfokus untuk belajar.
                </p>
            </li>
            
            <li>
                <p class="subtitle1">
                Mumpuni tentang ilmu agama.
                </p>
                <p>
                Saat bicara tentang Islam, ilmu anak pondok pesantren juga lebih mumpuni dan lebih bisa dipertanggungjawabkan. Manfaat anak berstatus santri pasti Ilmu agama didapatkan lebih bisa dipertanggungjawabkan. Ilmu agama Islam yang mereka dapatkan juga lebih mumpuni. Jika anak mondok dipesantren kelebihan dan manfaatnya akan terbentuk lebih baik untuk masa depan mendatang. Oleh karena itu bersyukurlah bagi orang tua yang masih diberikan kesempatan untuk bisa menyekolahkan anaknya di pondok pesantren
                </p>
            </li>
            
        </ul>

      </article>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
