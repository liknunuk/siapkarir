<div class="bg-dark text-light">
    <h3 class="sisPageHead">SIAP KARIR - MAN 2 BANJARNEGARA</h3>
</div>
<div class="container-fluid siswa">
    <div class="row sisrow">
        <div class="col-lg-2 sisid">
            <?php $this->view('siswa/siswaId',$data); ?>
        </div>
        <div class="col-lg-10 sisKonten">
            <h2 id="judul">
            <?php if($data['tipe']=='berita'){
                echo $data['info']['judul'];
            }else{
                echo $data['info']['namaAgenda'];
            }
            ?>
            </h2>
            <article>
            <?php 
            if($data['tipe']=='berita'){
                echo $data['info']['konten'];
                echo "<hr/>";
                echo $data['info']['nama'] .'-'. $data['info']['rilis'];
            }else{
                echo "
                    <table class='table table-sm table-bordered'>
                    <tbody>
                    <tr><td width='140'>Waktu</td><td>".$data['info']['waktu']."</td>
                    <tr><td>Tempat</td><td>".$data['info']['tempat']."</td>
                    <tr><td>Promotor</td><td>".$data['info']['namaPromotor']."</td>
                    <tr><td>Deskripsi</td><td>".$data['info']['deskripsi']."</td>
                    </tr>
                    </tbody>
                    </table>
                ";
            }
            ?>
            </article>
        </div>
    </div>
</div>