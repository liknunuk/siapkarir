<div class="container-fluid siswa">
    <div class="row sisrow">
        <div class="col-lg-2 sisid">
            <?php $this->view('siswa/siswaId',$data); ?>
        </div>
        <div class="col-lg-5 sisKonten">
            <div class="card">
                <div class="card-header sisCardHead">
                    <p>Berita Karir</p>
                </div>
                <div class="card-body list-group">
                    <?php foreach($data['berita'] as $berita): ?>
                        <li class="list-group-item sisBerita py-0">
                            <p class="beritaJudul py-0 my-0">
                            <a href="<?=BASEURL;?>Siswa/info/berita/<?=$berita['idBerita'];?>"><?=$berita['judul'];?></a></p>
                            <p class="beritaDetil py-0 my-0"><?=$berita['rilis'] .' - '.$berita['author'];?></p>
                        </li>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <div class="col-lg-5 sisKonten">
            <div class="card">
                <div class="card-header sisCardHead">
                    <p>Agenda Karir</p>
                </div>
                <div class="card-body">
                    <?php foreach($data['agenda'] as $agenda): ?>
                        <ul class="list-group mb-1">
                            <li class="list-group-item py-0">
                            <a href="<?=BASEURL;?>Siswa/info/agenda/<?=$agenda['idAgenda'];?>">
                                <?=$agenda['namaAgenda'];?></li>
                            </a>
                            <li class="list-group-item py-0"><?=$agenda['promotor'];?></li>
                            <li class="list-group-item py-0"><?=$agenda['waktu'];?> @ <?=$agenda['tempat'];?></li>
                        </ul>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        
    </div>
</div>

<?php $this->view('template/bs4js');?>