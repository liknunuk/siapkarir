<?php

$website = [
    'Kuliah'=>[
        "<a target='_blank' href='ltmpt.ac.id'>SNMPTN</a>",
        "<a target='_blank' href='web.snmptn.ac.id'>SBMPTN</a>",
        "<a target='_blank' href='https://um.undip.ac.id/'>UNDIP</a>",
        "<a target='_blank' href='http://penerimaan.unnes.ac.id/'>UNNES</a>",
        "<a target='_blank' href='https://spmb.uns.ac.id/'>UNS</a>",
        "<a target='_blank' href='http://spmb.unsoed.ac.id/'>UNSOED</a>",
        "<a target='_blank' href='https://um.untidar.ac.id/'>UNTID</a>",
        "<a target='_blank' href='https://spmb.isi-ska.ac.id/'>ISI Surakarta</a>",
        "<a target='_blank' href='https://um.ugm.ac.id/'>UGM</a>",
        "<a target='_blank' href='http://pmb.uny.ac.id/'>UNY</a>",
        "<a target='_blank' href='http://pmb.upnyk.ac.id/'>UPN Veteran</a>",
        "<a target='_blank' href='https://pmb.isi.ac.id/'>ISI Yogyakarta</a>",
        "<a target='_blank' href='https://admisi.ipb.ac.id/'>IPB</a>",
        "<a target='_blank' href='https://usm.itb.ac.id/'>ITB</a>",
        "<a target='_blank' href='https://pmb.upi.edu/'>UPI</a>",
        "<a target='_blank' href='http://smup.unpad.ac.id/'>UNPAD</a>",
        "<a target='_blank' href='https://selma.ub.ac.id/'>UNIBRAW</a>",
        "<a target='_blank' href='https://um.ac.id/akademik/seleksi-mahasiswa/jalur-seleksi-masuk/'>UNM</a>",
        "<a target='_blank' href='http://ppmb.unair.ac.id/'>UNAIR</a>",
        "<a target='_blank' href='https://smits.its.ac.id/'>ITS</a>",
        "<a target='_blank' href='https://sipenmaru.unesa.ac.id/'>UNNESA</a>",
        "<a target='_blank' href='span-ptkin.ac.id'>SPAN PTKIN</a>",
        "<a target='_blank' href='um.ptkin.ac.id'>UM PTKIN</a>",
        "<a target='_blank' href='http://pmb.walisongo.ac.id/'>UIN Semarang</a>",
        "<a target='_blank' href='https://admisi.uin-suka.ac.id/'>UIN Yogyakarta</a>",
        "<a target='_blank' href='https://spmb.uinjkt.ac.id/'>UIN Jakarta</a>",
        "<a target='_blank' href='https://pmb.uinsgd.ac.id/'>UIN Bandung</a>",
        "<a target='_blank' href='http://pmb.uin-malang.ac.id/'>UIN Malang</a>",
        "<a target='_blank' href='https://pmb.uinsby.ac.id/'>UIN Surabaya</a>",
        "<a target='_blank' href='http://iainpurwokerto.ac.id/'>IAIN Purwokerto</a>",
        "<a target='_blank' href='http://www.iainpekalongan.ac.id/'>IAIN Pekalogan</a>",
        "<a target='_blank' href='http://iainsalatiga.ac.id/'>IAIN Salatiga</a>",
        "<a target='_blank' href='http://www.iain-surakarta.ac.id/'>IAIN Surakarta</a>",
        "<a target='_blank' href='http://http//pmb.iainkudus.ac.id'>IAIN Kudus</a>",
        "<a target='_blank' href='snmpn.politehnik.or.id'>Politeknik Nasional</a>",
        "<a target='_blank' href='sbmpn.politehnik.or.id'>Politeknik Nasional</a>",
        "<a target='_blank' href='pmb.polines.ac.id'>Politehnik Negeri Semarang</a>",
        "<a target='_blank' href='pmb.pnc.ac.id'>Politehnik Negeri  Cilacap</a>",
        "<a target='_blank' href='https://simama-poltekkes.kemkes.go.id'>Poltekkes Semarang</a>",
        "<a target='_blank' href='https://sipenmaru.poltekkes-smg.ac.id'>Poltekkes Semarang</a>",
        "<a target='_blank' href='https://poltekkesjogja.ac.id/'>Poltekkes Yogyakarta</a>",
        "<a target='_blank' href='akpol.ac.id'>AKPOL</a>",
        "<a target='_blank' href='aau.ac.id'>AAU</a>",
        "<a target='_blank' href='aal.ac.id'>AAL</a>",
        "<a target='_blank' href='akmil.ac.id'>AKMIL</a>",
        "<a target='_blank' href='spmb.pkn.stan.ac.id'>STAN</a>",
        "<a target='_blank' href='ipdn.ac.id'>IPDN</a>",
        "<a target='_blank' href='https://stsn-nci.ac.id/'>STSN</a>",
        "<a target='_blank' href='sip.ac.id'>SIP</a>",
        "<a target='_blank' href='mmtc.ac.id'>MMTC</a>",
        "<a target='_blank' href='stmkg.ac.id'>STMKG</a>",
        "<a target='_blank' href='sttd.ac.id'>STTD</a>",
        "<a target='_blank' href='stpi.ac.id'>STPI</a>",
        "<a target='_blank' href='stin.ac.id'>STIN</a>",
        "<a target='_blank' href='stis.ac.id'>STIS</a>"

    ],
    'Kursus' =>[
        "<a target='_blank' href='https://balatkertransklampok.wordpress.com/'>BLK Klampok</a>",
        "<a target='_blank' href='https://www.facebook.com/groups/1435771383361706/'>BLK Cilacap</a>",
        "<a target='_blank' href='https://blk.wonosobokab.go.id/home/'>BLK Wonosobo</a>"

    ],

    'Kerja'=>[
        "<a target='_blank' href='https://www.jobstreet.co.id/'>Job Street</a>",
        "<a target='_blank' href='https://www.karir.com/'>Karir dot com</a>",
        "<a target='_blank' href='https://www.qerja.com/'>Qerja dot com</a>",
        "<a target='_blank' href='https://www.topkarir.com/'>Top Karir</a>",
        "<a target='_blank' href='https://www.hiredtoday.com/'>Hired Today</a>",
        "<a target='_blank' href='https://adakerja.com/'>Ada Kerja dot com</a>",
        "<a target='_blank' href='https://www.loker.id/'>Loker dot id</a>",
        "<a target='_blank' href='https://www.urbanhire.com/'>Urbanhire</a>",
        "<a target='_blank' href='https://www.kalibrr.id/'>Kalibrr Indonesia</a>",
        "<a target='_blank' href='https://glints.com/id/'>Glints Indonesia</a>"
    ],
    'Mondok'=>[
        "<a target='_blank' href='https://kangsantri.id/27-pondok-pesantren-kualitas-di-indonesia/'>Kang Santri dot id</a>",
        "<a target='_blank' href='https://pontren.com/2019/01/15/daftar-pondok-pesantren-terbaik-di-jawa-tengah/'>Pontren dot com</a>",
        "<a target='_blank' href='https://www.infopesantren.com/2019/01/pesantren-di-jawa-tengah.html'>Info Pesantren dot com</a>"

    ],
    'Wirausaha'=>[
        "<a target='_blank' href='https://dinindagkop.banjarnegarakab.go.id/'>Dinas Indagkop Banjarnegara</a>",
        "<a target='_blank' href='www.tangandiatas.com'>Tangan di atas dot com</a>",
        "<a target='_blank' href='https://www.entrepreneuruniversity-eu.com/'>Entrepreneur University</a>",
        "<a target='_blank' href='www.hipmi.org'>Himpunan Pengusaha Muda Indonesia</a>"
    ]
];

?>
<div class="bg-dark text-light">
    <h3 class="sisPageHead">SIAP KARIR - MAN 2 BANJARNEGARA</h3>
</div>
<div class="container-fluid siswa">
    <div class="row sisrow">
        <div class="col-lg-2 sisid">
            <?php $this->view('siswa/siswaId',$data); ?>
        </div>
        <div class="col-lg-10 sisKonten">
            <h3>Website Rujukan Karir-mu!</h3>
            <div class="list-group">
                <?php
                $karir = $_SESSION['karir'];
                foreach( $website[$karir] as $karir ){
                    echo "<li class='list-group-item'>$karir</li>";
                }
                ?>
            </div>
        </div>
    </div>
</div>