<div class="container-fluid mitra">
    <div class="row">
        <div class="col-lg-12 mtrTitle">
            <h3>Mitra SIAP Karir MAN 2 Banjarnegara</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 mtrContent">
            <div class="form-group center-xy">
                <label for="mtrTelepon">Nomor Telepon</label><br/>
                <input type="password" id="mtrTelepon" value="022 5567 897"><br/>
                <span id="loginFailed"></span>
            </div>
        </div>
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
    $('#mtrTelepon').keypress( function(e){
        if(e.keyCode == 13){
            e.preventDefault();
            let phone =  $(this).val();
            $.post('<?=BASEURL.'Login/mtrAuth';?>',{
                telepon: phone
            }, function(resp){
                // $('#loginFailed').text(resp);
                if(resp == '1'){
                    $('#loginFailed').text(resp);
                    window.location = "<?=BASEURL.'Mitra';?>";
                }else{
                }
            })
        }
    })
</script>