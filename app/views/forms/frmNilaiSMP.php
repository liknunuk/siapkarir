<?php
$mapel=[
    'abp'=>'Pend. Agama dan Budi Pekerti',
    'pkn'=>'Pend. Pancasila dan Kewarganegaraan',
    'ind'=>'Bahasa Indonesia',
    'ing'=>'Bahasa Inggris',
    'mtk'=>'Matematika',
    'ipa'=>'Ilmu Pengetahuan Alam',
    'ips'=>'Ilmu Pengetahuan Sosial',
    'sbk'=>'Seni Budaya',
    'pjk'=>'Pendidikan Jasmani, Olah Raga dan Kesehatan',
    'mlk'=>'Muatan Lokal',
    'aqh'=>'Alquran dan Hadits',
    'aqa'=>'Aqidah dan Akhlak',
    'fqh'=>'Fiqh',
    'ski'=>'Sejarah Kebudayaan Islam',
    'arb'=>'Bahasa Arab',
];
?>
<form action="<?=$data['action'];?>" method="post" class="form-horizontal">
    <div class="form-group row">
        <label for="nsmpNis" class="col-sm-4">Nomor Induk Siswa Nasional</label>
        <div class="col-sm-8">
            <input type="text" name="nis" id="nsmpNis" class="form-control" value="<?=$data['nis'];?>">
        </div>
    </div>

    <?php foreach($mapel as $km=>$nm): ?>
    <div class="form-group row">
        <label for="nsmp<?=ucfirst($km);?>" class="col-sm-4"><?=$nm;?></label>
        <div class="col-sm-8">
            <input type="text" name="<?=$km;?>" id="nsmp<?=ucfirst($km);?>" class="form-control"  maxlength="6" value="<?=$data['nilai'][$km];?>">
        </div>
    </div>
    <?php endforeach; ?>

    <div class="form-group row d-flex justify-content-end">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>