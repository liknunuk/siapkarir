<!-- idBerita,rilis,author,judul,konten,tags -->
<form action="<?=$data['action'];?>" method="post" class="form-horizontal">
    
    <div class="form-group row">
        <label for="brtID" class="col-sm-3">ID Berita</label>
        <div class="col-sm-9">
            <input type="text" name="idBerita" id="brtID" class="form-control" readonly value="<?=$data['berita']['idBerita'];?>" >
        </div>
    </div>
    
    <div class="form-group row">
        <label for="brtRilis" class="col-sm-3">Tanggal Rilis</label>
        <div class="col-sm-9">
            <input type="date" name="rilis" id="brtRilis" class="form-control" value="<?=$data['berita']['rilis'];?>" >
        </div>
    </div>
    <!--     
    <div class="form-group row">
        <label for="brtAuth" class="col-sm-3">Mitra Penulis</label>
        <div class="col-sm-9">
     -->
            <input type="hidden" name="author" id="brtAuth" class="form-control" readonly  value="<?=$_SESSION['mitraID'];?>" >
    <!-- 
        </div>
    </div>
     -->
    <div class="form-group row">
        <label for="brtJudul" class="col-sm-3">Judul</label>
        <div class="col-sm-9">
            <input type="text" name="judul" id="brtJudul" class="form-control" value="<?=$data['berita']['judul'];?>" >
        </div>
    </div>
    
    <div class="form-group row">
        <label for="brtKonten" class="col-sm-3">Isi Berita</label>
        <div class="col-sm-9">
            <textarea name="konten" id="brtKonten" class="form-control tmce" rows=20><?=$data['berita']['konten'];?></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="brtTag" class="col-sm-3">Tags</label>
        <div class="col-sm-9">
            <input type="text" name="tags" id="brtTag" class="form-control" placeholder="Pisahkan dengan tanda koma" value="<?=$data['berita']['tags'];?>" >
        </div>
    </div>

    <div class="form-group d-flex justify-content-end px-3">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>

</form>