<!-- idAgenda,promotor,namaAgenda,waktu,tempat,deskripsi -->
<form action="<?=$data['action'];?>" method="post" class="form-horizontal">
    <input type="hidden" name="idAgenda" id="agdID" value="">
    <input type="hidden" name="promotor" id="agdPromotor" value="<?=$data['idsh'];?>">

    <div class="form-group row">
        <label for="agdNama" class="col-sm-2">Nama Agenda</label>
        <div class="col-sm-10">
            <input type="text" name="namaAgenda" id="agdNama" class="form-control">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="agdWaktu" class="col-sm-2">Waktu</label>
        <div class="col-sm-5">
            <input type="date" name="tanggal" id="agdWaktu" class="form-control">
        </div>
        <div class="col-sm-5">
            <input type="time" name="waktu" id="agdWaktu" class="form-control">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="agdTempat" class="col-sm-2">Tempat</label>
        <div class="col-sm-10">
            <input type="text" name="tempat" id="agdTempat" class="form-control">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="agdDeskripsi" class="col-sm-2">Deskripsi Kegiatan</label>
        <div class="col-sm-10">
            <input type="text" name="deskripsi" id="agdDeskripsi" class="form-control">
        </div>
    </div>
    
    <div class="form-group d-flex justify-content-end px-3">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>