<!-- idsh,nama,kategori,contactPerson,telepon,email,website -->
<form action="<?=$data['action'];?>" method="post" class="form-horizontal">

    <div class="form-group row">
        <label for="shID" class="col-sm-4">ID Mitra</label>
        <div class="col-sm-8">
            <input type="text" name="idsh" id="shID" class="form-control" readonly value="<?=$data['mitra']['idsh'];?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="shNama" class="col-sm-4">Nama Lembaga Mitra</label>
        <div class="col-sm-8">
            <input type="text" name="nama" id="shNama" class="form-control" value="<?=$data['mitra']['nama'];?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="shCat" class="col-sm-4">Kategori</label>
        <div class="col-sm-8">
            <?php echo $this->model('Model_options')->karir(); ?>
        </div>
    </div>

    <div class="form-group row">
        <label for="shCP" class="col-sm-4">Kontak Person</label>
        <div class="col-sm-8">
            <input type="text" name="contactPerson" id="shCP" class="form-control" value="<?=$data['mitra']['contactPerson'];?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="shAlamat" class="col-sm-4">Alamat</label>
        <div class="col-sm-8">
            <input type="text" name="alamat" id="shAlamat" class="form-control" value="<?=$data['mitra']['alamat'];?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="shTelp" class="col-sm-4">No. Telepon</label>
        <div class="col-sm-8">
            <input type="text" name="telepon" id="shTelp" class="form-control" value="<?=$data['mitra']['telepon'];?>">
        </div>
    </div>

    <div class="form-group row">
        <label for="shMail" class="col-sm-4">Alamat Surel</label>
        <div class="col-sm-8">
            <input type="email" name="email" id="shMail" class="form-control" value="<?=$data['mitra']['email'];?>" >
        </div>
    </div>

    <div class="form-group row">
        <label for="shWeb" class="col-sm-4">Website Resmi</label>
        <div class="col-sm-8">
            <input type="text" name="website" id="shWeb" class="form-control" value="https://<?=$data['mitra']['website'];?>" >
        </div>
    </div>

    <div class="form-group d-flex justify-content-end px-3">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>