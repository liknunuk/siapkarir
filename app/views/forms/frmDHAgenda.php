<!-- idAgenda,promotor,namaAgenda,waktu,tempat,deskripsi -->
<form action="" method="post" class="form-horizontal">
    <input type="hidden" name="idAgenda" id="agdID" value="">

    <div class="form-group row">
        <label for="agdNama" class="col-sm-2">Nama Agenda</label>
        <div class="col-sm-10">
            <input type="text" name="namaAgenda" id="agdNama" class="form-control" readonly>
        </div>
    </div>

    <div class="form-group row">
        <label for="agdNisn" class="col-sm-2">NISN</label>
        <div class="col-sm-10">
            <input type="text" name="siswaHadir" id="agdNisn" class="form-control">
        </div>
    </div>
    
    
    <div class="form-group d-flex justify-content-end px-3">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>