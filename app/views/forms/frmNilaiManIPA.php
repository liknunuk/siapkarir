<?php
$mapelA=[
    'a_aqhd'=>'Alquran dan Hadits',
    'a_aqkh'=>'Akidah Akhlak',
    'a_barb'=>'Bahasa Arab',
    'a_bind'=>'Bahasa Indonesia',
    'a_bing'=>'Bahasa Inggris',
    'a_fiqh'=>'Fiqh',
    'a_mttk'=>'Matematika',
    'a_ppkn'=>'Pend. Pancasila dan Kewarganegaraan',
    'a_sjid'=>'Sejarah Indonesia',
    'a_skbi'=>'Sejarah Kebudayaan Islam'
];
$mapelB=[
    'b_pjok'=>'Pend. Jasmani Olah Raga dan Kesehatan',
    'b_pkwu'=>'Prakarya dan Kewirausahaan',
    'b_sbdy'=>'Seni Budaya'
];
$mapelC=[
    'ca_bio'=>'Biologi',
    'ca_fsk'=>'Fisika',
    'ca_kim'=>'Kimia',
    'ca_mtk'=>'Matematika'
];

if(!empty($data['kodeNilai'])){
    list($nis,$smt)=explode("_",$data['kodeNilai']);
}else{
    $nis = 0; $smt=0;
}

function ceksem($a,$b){
    if($a == $b){
        echo "selected";
    }else{
        echo '';
    }
}
?>

<form action="<?=$data['action'];?>" method="post" class="form-horizontal">
    <div class="form-group row">
        <label for="nmanNis" class="col-sm-4">Nomor Induk Siswa Nasional</label>
        <div class="col-sm-8">
            <input type="text" name="nis" id="nmanNis" class="form-control" value="<?=$nis;?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="nmanSmt" class="col-sm-4">Semester</label>
        <div class="col-sm-8">
            <select name="semester" id="nmanSmt" class="form-control">
                <option value="1" <?php ceksem($nis,1);?>>Semester 1</option>
                <option value="2" <?php ceksem($nis,2);?>>Semester 2</option>
                <option value="3" <?php ceksem($nis,3);?>>Semester 3</option>
                <option value="4" <?php ceksem($nis,4);?>>Semester 4</option>
                <option value="5" <?php ceksem($nis,5);?>>Semester 5</option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="nmanKdNilai" class="col-sm-4">KodeNilai</label>
        <div class="col-sm-8">
            <input type="text" name="kodeNilai" id="nmanKdNilai" class="form-control" value="<?=$data['kodeNilai'];?>">
        </div>
    </div>

    <div class="form-group">
    <h4>Kelompok A</h4>
    </div>
    <?php foreach($mapelA as $km=>$nm): ?>
    <div class="form-group row">
        <label for="mpmaa<?=ucfirst($km);?>" class="col-sm-4"><?=$nm;?></label>
        <div class="col-sm-8">
            <input type="text" name="<?=$km;?>" id="mpmaa<?=ucfirst($km);?>" class="form-control nilai" maxlength="7" placeholder="Nilai Pengetahuan/Nilai Keterampilan" value="<?=$data['nilai'][$km];?>">
        </div>
    </div>
    <?php endforeach; ?>

    <div class="form-group">
    <h4>Kelompok B</h4>
    </div>
    <?php foreach($mapelB as $km=>$nm): ?>
    <div class="form-group row">
        <label for="mpmaa<?=ucfirst($km);?>" class="col-sm-4"><?=$nm;?></label>
        <div class="col-sm-8">
            <input type="text" name="<?=$km;?>" id="mpmaa<?=ucfirst($km);?>" class="form-control nilai" maxlength="7" placeholder="Nilai Pengetahuan/Nilai Keterampilan" value="<?=$data['nilai'][$km];?>">
        </div>
    </div>
    <?php endforeach; ?>

    <div class="form-group">
    <h4>Kelompok C</h4>
    </div>
    <?php foreach($mapelC as $km=>$nm): ?>
    <div class="form-group row">
        <label for="mpmaa<?=ucfirst($km);?>" class="col-sm-4"><?=$nm;?></label>
        <div class="col-sm-8">
            <input type="text" name="<?=$km;?>" id="mpmaa<?=ucfirst($km);?>" class="form-control nilai" maxlength="7" placeholder="Nilai Pengetahuan/Nilai Keterampilan" value="<?=$data['nilai'][$km];?>">
        </div>
    </div>
    <?php endforeach; ?>

    <div class="form-group row d-flex justify-content-end">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>
<?php $this->view('template/bs4js'); ?>
<script>
$("#nmanSmt").change( function(){
    let nis,smt;
    nis=$("#nmanNis").val();
    smt=$("#nmanSmt").val();
    $("#nmanKdNilai").val(nis+'_'+smt);
})
$(".nilai").blur( function(){
    let nilai = this.value;
    let regex = /[0-9]{2,3}\/[0-9]{2,3}/g;
    if( regex.test(nilai) == false ){
        $(this).focus();
        $(this).val('');
    }
    let biji = nilai.split('/');
    let n1 = parseInt(biji[0]);
    let n2 = parseInt(biji[1]);
    if(n1 >100 || n2 >100){
        $(this).focus();
        $(this).val('');
    }
})
// .match(/[0-9]{2,3}\/[0-9]{2,3}/g)
</script>