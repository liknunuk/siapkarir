<!-- nis,nama,jnsKelamin,tempatLahir,tanggalLahir,jurusan,rencanaKarir,kepribadian -->
<form action="" class="form-horizontal" method="post">
    
    <div class="form-group row">
        <label for="sisNis" class="col-sm-4">Nomor Induk SisNiswa</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="sisNis" name="nis" value="<?=0;?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisNama" class="col-sm-4">Nama Lengkap</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="sisNama" name="nama" value="<?=0;?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisJnk" class="col-sm-4">Jenis Kelamin</label>
        <div class="col-sm-8">
            <!-- ambil dari view options -->
            <?php echo $this->model('Model_options')->kelamin(); ?>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisTpLahir" class="col-sm-4">Tempat Lahir</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="sisTpLahir" name="tempatLahir" value="<?=0;?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisTgLahir" class="col-sm-4">Tanggal Lahir</label>
        <div class="col-sm-8">
            <input type="date" class="form-control" id="sisTgLahir" name="tanggalLahir" value="<?=0;?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisJurusan" class="col-sm-4">Jurusan</label>
        <div class="col-sm-8">
            <!-- ambil dari view options -->
            <?php echo $this->model('Model_options')->jurusan(); ?>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisRenKar" class="col-sm-4">Rencana Karir</label>
        <div class="col-sm-8">
            <!-- ambil dari view options -->
            <?php echo $this->model('Model_options')->karir(); ?>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="sisPribadi" class="col-sm-4">Kepribadian</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" id="sisPribadi" name="kepribadian" value="<?=0;?>">
        </div>
    </div>
    
    <div class="form-group d-flex justify-content-end">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>
</form>

