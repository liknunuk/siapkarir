<!-- nis,orangTua,teman,saudara,kolega,gurubk -->
<form action="<?=$data['action'];?>" method="post" class="form-horizontal">

    <div class="form-group row">
        <label for="rekoNis" class="col-sm-4">Nomor Induk Siswa Nasional</label>
        <div class="col-sm-8">
            <input type="text" name="nis" id="rekoNis" class="form-control" value="<?=$data['rekom']['nis'];?>">
        </div>
    </div>
    
    <div class="form-group row">
        <label for="rekoTeman" class="col-sm-4">Teman Sebaya</label>
        <div class="col-sm-8">
            <textarea name="teman" id="rekoTeman" rows="5" style="resize:none;" class="form-control"><?=$data['rekom']['teman'];?></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="rekoOrtu" class="col-sm-4">Orang Tua</label>
        <div class="col-sm-8">
            <textarea name="orangTua" id="rekoOrtu" rows="5" style="resize:none;" class="form-control"><?=$data['rekom']['orangTua'];?></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="rekoSdr" class="col-sm-4">Saudara</label>
        <div class="col-sm-8">
            <textarea name="saudara" id="rekoSdr" rows="5" style="resize:none;" class="form-control"><?=$data['rekom']['saudara'];?></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="rekoGBK" class="col-sm-4">Guru BK</label>
        <div class="col-sm-8">
            <textarea name="gurubk" id="rekoGBK" rows="5" style="resize:none;" class="form-control"><?=$data['rekom']['gurubk'];?></textarea>
        </div>
    </div>
    
    <div class="form-group row">
        <label for="rekoKolega" class="col-sm-4">Kolega/Rekanan</label>
        <div class="col-sm-8">
            <textarea name="kolega" id="rekoKolega" rows="5" style="resize:none;" class="form-control"><?=$data['rekom']['kolega'];?></textarea>
        </div>
    </div>

    <div class="form-group d-flex justify-content-end px-3">
        <button type="submit" class="btn btn-primary">Simpan</button>
    </div>

</form>