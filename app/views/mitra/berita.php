<div class="container-fluid mitra">
    <div class="row">
        <div class="col-lg-12 mtrTitle">
            <h3>Mitra SIAP Karir MAN 2 Banjarnegara</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-2 mtrIdentity">
            <?php $this->view('mitra/identity',$data); ?>
        </div>
        <div class="col-lg-10 mtrContent">
            <div class="pageTitle">
                <h3>
                    <?=$data['berita']['judul'];?>
                </h3>
            </div>
            <p>Rilis: <?=$data['berita']['rilis'];?></p>
            <article>
                <?=$data['berita']['konten']; ?>
            </article>
            <p>Tags:
             <?php 
            $tags = explode(",",$data['berita']['tags']);
            foreach($tags as $i=>$tag):
            ?>
            <a href="javascript:void(0)"><?=$tag;?></a>&nbsp;
            <?php endforeach; ?>
            </p>
        </div>
    </div>
</div>