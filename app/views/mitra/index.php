<div class="container-fluid mitra">
    <div class="row">
        <div class="col-lg-12 mtrTitle">
            <h3>Mitra SIAP Karir MAN 2 Banjarnegara</h3>
        </div>
    </div>
    <div class="row" style="min-height: 80vh;">
    <div class="col-lg-2 mtrIdentity">
        <?php $this->view('mitra/identity',$data);?>
    </div>
        <div class="col-lg-10 mtrContent">
            <div class="pageTitle">
                <h3>BERITA-BERITA <?=$data['identity']['nama'];?></h3>
            </div>
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-borderd table-manduba">
                <thead>
                    <tr>
                        <th>Tgl. Rilis</th>
                        <th>Judul</th>
                        <th>Tags</th>
                        <th>
                            <!-- 
                                <a href="javascript:void(0)" class="brtEdhit"> <i class="fa fa-newspaper"></i> </a>
                                <a href="javascript:void(0)" class="brtHapus"> <i class="fa fa-trash"></i> </a> 
                            -->
                            <i class="fa fa-gears"></i>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($data['berita'] as $berita): ?>
                        <tr>
                            <td><?=$berita['rilis'];?></td>
                            <td><?=$berita['judul'];?></td>
                            <td>
                                <?php 
                                $tags = explode(",",$berita['tags']);
                                foreach($tags as $i=>$tag):
                                ?>
                                <a href="javascript:void(0)"><?=$tag;?></a>&nbsp;
                                <?php endforeach; ?>
                            </td>
                            <td>
                                <a href="<?=BASEURL;?>Mitra/berita/<?=$berita['idBerita'];?>">
                                    <i class="fa fa-edit"></i>
                                </a> | 
                                <a href="<?=BASEURL;?>Mitra/beritane/<?=$berita['idBerita'];?>">
                                    <i class="fa fa-newspaper"></i>
                                </a> | 
                                <a href="javascript:void(0)" class='brtHapus' id="<?=$berita['idBerita'];?>">
                                    <i class="fa fa-trash"></i>
                                </a> |

                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php $this->view('template/bs4js'); ?>