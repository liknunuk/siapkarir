<nav class="navbar navbar-expand-lg navbar-dark bg-violet">
  <a class="navbar-brand" href="<?=BASEURL;?>Gbk">SIAP Karir MAN 2 Banjarnegara</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarBinaka" aria-controls="navbarBinaka" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarBinaka">
    <ul class="navbar-nav ml-auto">
      
      <li class="nav-item">
        <a class="nav-link" href="<?=BASEURL;?>Gbk/siswa">Siswa</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?=BASEURL;?>Gbk/mitra">Mitra</a>
      </li>
      
      <li class="nav-item">
        <a class="nav-link" href="<?=BASEURL;?>Gbk/agenda">Agenda</a>
      </li>
      
    </ul>
  </div>
</nav>

