<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/gbk-menus');?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>Daftar Siswa MAN 2</h3>
            </div>
            <?php Alert::sankil(); ?>
            <div class="table-responsive">
                <table class="table table-sm-table-bordered table-manduba">
                    <thead>
                        <tr>
                            <th>NISN</th>
                            <th>NAMA LENGKAP</th>
                            <th>JENIS KELAMIN</th>
                            <th>JURUSAN</th>
                            <th>RENCANA KARIR</th>
                            <th>AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['siswa'] as $siswa ): ?>
                            <tr>
                                <td><?=$siswa['nis'];?></td>
                                <td><?=$siswa['nama'];?></td>
                                <td><?=$siswa['jnsKelamin'];?></td>
                                <td><?=$siswa['jurusan'];?></td>
                                <td><?=$siswa['rencanaKarir'];?></td>
                                <td>
                                    <a href="<?=BASEURL."Gbk/profil/".$siswa['nis'];?>" class="btn btn-primary">Profil</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- </div> -->
<?php $this->view('template/bs4js'); ?>