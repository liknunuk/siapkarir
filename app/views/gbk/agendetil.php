<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/agdbk-menus'); ?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>Informasi Agenda #<?=$data['idsh'];?></h3>
            </div>
                <table class="table table-sm table-bordered table-manduba">
                    <tbody>
                        <tr>
                            <th>ID Agenda</th>
                            <td><?=$data['acara']['idAgenda'];?></td>
                        </tr>
                        <tr>
                            <th>Nama Kegiatan</th>
                            <td><?=$data['acara']['namaAgenda'];?></td>
                        </tr>
                        <tr>
                            <th>Waktu</th>
                            <td><?=$data['acara']['waktu'];?></td>
                        </tr>
                        <tr>
                            <th>Tempat Penyelenggaraan</th>
                            <td><?=$data['acara']['tempat'];?></td>
                        </tr>
                        <tr>
                            <th>Penyelenggara</th>
                            <td><?=$data['acara']['namaPromotor'];?></td>
                        </tr>
                        <tr>
                            <th>Deskripsi</th>
                            <td><?=$data['acara']['deskripsi'];?></td>
                        </tr>
                    </tbody>
                </table>
        </div>
    </div>
<!-- </div> -->

<!-- 

    [idAgenda] => 1
    [promotor] => 3
    [namaAgenda] => Workshop Marketing Digital
    [waktu] => 2020-11-25 09:00:00
    [tempat] => LKP Pikom
    [deskripsi] => Pelatihan marketing digital menggunakan Instagram Bussines
    [siswaHadir] => 
    [namaPromotor] => LKP Pikom

 -->