<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/main-menus'); ?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>SIAP KARIR MANDUBARA</h3>
            </div>
            <table class="table table-sm table-manduba">
                <thead>
                    <tr>
                        <th colspan="5">Peta Sebaran Pilihan Karir</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <?php foreach($data['karir'] as $karir): ?>
                        <td width="20%">
                            <div class="card">
                                <div class="card-body karir-body text-center">
                                    <h2>
                                        <?=$karir['jumlah'];?>
                                    </h2>
                                </div>
                                <div class="card-header karir-header text-center">
                                    <?=$karir['karir'];?>
                                </div>
                            </div>
                        </td>
                    <?php endforeach; ?>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <?php print_r($_SESSION); ?>
<!-- </div> -->
<?php $this->view('template/bs4js'); ?>
