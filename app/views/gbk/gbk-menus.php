<div class="col-lg-2" id="bkMenus">
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" href="<?=BASEURL.'Gbk/siswabaru';?>">Input Siswa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=BASEURL.'Gbk/nilaimasuk';?>">Input Nilai SLTP</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=BASEURL.'Gbk/nilaiSmtIpa';?>">Input Nilai Semester IPA</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?=BASEURL.'Gbk/nilaiSmtIps';?>">Input Nilai Semester IPS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?=BASEURL.'Gbk/nilaiSmtBhs';?>">Input Nilai Semester Bahasa</a>
        </li>
        <li class="nav-item">
            <a class="nav-link disabled" href="<?=BASEURL.'Gbk/nilaiSmtAgm';?>">Input Nilai Semester Agama</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?=BASEURL.'Gbk/rekomendasi';?>">Input Rekomendasi</a>
        </li>
    </ul>
</div>