<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/agdbk-menus'); ?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>Agenda Bina Karir</h3>
            </div>
            <div class="table-responsive">
                <?php Alert::sankil(); ?>
                <table class="table table-sm table-striped table-manduba">
                    <thead>
                        <tr>
                            <th>Nama Agenda</th>
                            <th>Promotor</th>
                            <th>Waktu</th>
                            <th>Tempat</th>
                            <th>Detil</th>
                        </tr>
                    </thead>
                    <tbody>
                    <!-- idAgenda , promotor , namaAgenda , waktu , tempat , deskripsi  -->
                        <?php foreach($data['acara'] as $acara): ?>
                        <tr>
                            <td>
                                <?=$acara['namaAgenda'];?><br/>
                                <?=$acara['kategori'];?>
                            </td>
                            <td><?=$acara['promotor'];?></td>
                            <td><?=$acara['waktu'];?></td>
                            <td><?=$acara['tempat'];?></td>
                            <td>
                                <a href="<?=BASEURL.'Gbk/dagenda/'.$acara['idAgenda'];?>">
                                    <i class="fa fa-list"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<!-- </div> -->
<?php $this->view('template/bs4js'); ?>

<script>
    $(document).ready( function(){
        $('#srcidsh').keyup( function(){
            let promotor = $(this).val();
            if ( promotor.length >=3 ){
                $.getJSON('<?=BASEURL;?>Gbk/promotorlist/'+promotor , function(resp){
                    $('#promotors option').remove();
                    $('#promotors').show();
                    $.each( resp , function(i,data){
                        $('#promotors').append(`
                        <option value='${data.idsh}'>${data.nama}</option>
                        `);
                    })
                })
            }else{
                $("#promotors").hide();

            }
        })

        $('#promotors').on('click','option',function(){
            window.location.href=`<?=BASEURL;?>Gbk/agendabaru/`+$(this).val();
        })
    })
</script>