<?php
    if($data['page'] == 1){
        $np = $data['page'] + 1;
        $pp = 1;
    }else{
        $np = $data['page'] + 1;
        $pp = $data['page'] - 1;
    }
?>
<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/mitragbk-menus'); ?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>MITRA BIMBINGAN KARIR MAN 2 BANJARNEGARA</h3>
            </div>
            <!-- tabel -->
            <?php Alert::sankil(); ?>
            <div class="table-responsive">
                <table class="table table-sm table-bordered table-manduba">
                    <thead>
                        <tr>
                            <th>NAMA MITRA</th>
                            <th>KATEGORI</th>
                            <th>INFORMASI</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($data['mitra'] as $mitra ): ?>
                        <tr>
                            <td><?=$mitra['nama'];?></td>
                            <td><?=$mitra['kategori'];?></td>
                            <td>
                                <p style="margin:0;padding:0;"><span class="mitraInfo">Contact Person</span> <?=$mitra['contactPerson'];?></p>
                                <p style="margin:0;padding:0;"><span class="mitraInfo">Telepon</span> <?=$mitra['telepon'];?></p>
                                <p style="margin:0;padding:0;"><span class="mitraInfo">Alamat</span> <?=$mitra['alamat'];?></p>
                                <p style="margin:0;padding:0;"><span class="mitraInfo">Email</span> <a href="mailto:<?=$mitra['email'];?>" ><?=$mitra['email'];?></a></p>
                                <p style="margin:0;padding:0;"><span class="mitraInfo">Website</span> <a href="<?=$mitra['website'];?>" target="_blank" ><?=$mitra['website'];?></a></p>
                            </td>
                            <td>
                                <a href="<?=BASEURL;?>Gbk/mitralama/<?=$mitra['idsh'];?>" class="btn btn-success">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="javascript:void(0)" id="<?=$mitra['idsh'];?>" class="btn btn-success btn-delete">
                                    <i class="fa fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <!-- pagination -->
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Gbk/mitra/<?=$pp;?>" >Previous</a></li>
                        <li class="page-item"><a class="page-link" href="#"><?=$data['page'];?></a></li>
                        <li class="page-item"><a class="page-link" href="<?=BASEURL;?>Gbk/mitra/<?=$np;?>" >Next</a></li>
                    </ul>
                </nav>
                <!-- pagination -->
            </div>
            <!-- tabel -->
        </div>
    </div>
<!-- </div> -->
<?php $this->view('template/bs4js'); ?>
<script>
    $(".btn-delete").click( function(){
        let id=$(this).prop('id');
        let tenan = confirm("Data akan dihapus permanen!");
        if ( tenan == true ){
            $.post('<?=BASEURL;?>Gbk/rmvMitra',{
                idsh: id
            }, function(resp){
                if( resp == "1" ){
                    location.reload();
                }
            })
        }
    })
</script>