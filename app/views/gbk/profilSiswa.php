<?php
$mapelsmp = [];
$mapelman = [];
foreach ($data['mpsmp'] as $mpsmp ) {
    $kode = $mpsmp['kode'];
    $mapelsmp[$kode]=$mpsmp['arti'];
}
foreach ($data['mpman'] as $mpman ) {
    $kode = $mpman['kode'];
    $mapelman[$kode]=$mpman['arti'];
}
?>
<div class="container-fluid" style="height:100%">
    <div class="row" id="gbkMain">
        <?php $this->view('gbk/gbk-menus'); ?>
        <div class="col-lg-10">
            <div class="pageTitle">
                <h3>PROFIL SISWA : <?=$data['nisn'];?></h3>
            </div>
            <div class="card">
                <div class="card-header"><h3>Identitas Siswa</h3></div>
                <div class="card-body">                    
                    <div class="row">
                        <div class="col-md-3 prof-l">Nomor Induk</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['nis'];?></div>
                        <div class="col-md-3 prof-l">Nama Lengkap</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['nama'];?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 prof-l">Tempat Lahir</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['tempatLahir'];?></div>
                        <div class="col-md-3 prof-l">Tgl Lahir</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['tanggalLahir'];?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 prof-l">Jurusan</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['jurusan'];?></div>
                        <div class="col-md-3 prof-l">Rencana Karir</div>
                        <div class="col-md-3 prof-r"><?=$data['siswa']['rencanaKarir'];?></div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 prof-l">Kepribadian</div>
                        <!-- <div class="col-md-9 prof-r"><?=$data['siswa']['kepribadian'];?></div> -->
                        <div class="col-md-9 prof-r" id="dapri"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h3>Nilai Masuk MAN 2</h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>Mata Pelajaran</th>
                                        <th>Nilai</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        ksort($data['nismp']);
                                        foreach($data['nismp'] as $mpsmp=>$nismp): 
                                    ?>
                                    <?php if($mpsmp=='nis' || $nismp=='0.00'){ continue; } ?>
                                        <tr>
                                            <td><?=$mapelsmp[$mpsmp] ;?></td>
                                            <td class='text-right'><?=$nismp;?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php for($smt = 1 ; $smt <= $data['nilai']['semester'] ; $smt++):?>
                
                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                            <h3>Nilai Semester <?=$smt;?></h3>
                        </div>
                        <div class="card-body">
                            <table class="table table-sm table-striped">
                                <thead>
                                    <tr>
                                        <th>Mata Pelajaran</th>
                                        <th>Pengetahuan</th>
                                        <th>Ketrampilan</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                    ksort($data['nsmt'.$smt]);
                                    foreach($data['nsmt'.$smt] as $mpsmt=>$nsmt):
                                    list($peng,$skil) = explode("/",$nsmt);
                                    if($nsmt == '000/000' || $mpsmt == 'kodeNilai') continue;
                                ?>
                                    <tr>
                                        <td><?=$mapelman[$mpsmt];?></td>
                                        <td class="text-right"><?=$peng;?></td>
                                        <td class="text-right"><?=$skil;?></td>
                                    </tr>

                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                
                <?php endfor; ?>

                <div class="col-sm-6">
                    <div class="card">
                        <div class="card-header">
                        <h3>Rekomendasi</h3>
                        </div>
                        <div class="card-body">
                            <!-- orangTua,teman,saudara,kolega,gurubk -->
                            <b>"Orang Tua"</b>
                            <p>
                                <?php
                                    if(empty($data['rekom']['orangTua'])){
                                        for($i = 1 ; $i <=5 ;$i++ ){
                                            echo "Sample rekomendasi orang tua. ";
                                        }
                                        echo "Rekomendasi belum terisi .<br/>";
                                    }else{
                                        echo $data['rekom']['orangTua'];
                                    }
                                ?>
                            </p>
                            <b>"Saudara"</b>
                            <p>
                                <?php
                                    if(empty($data['rekom']['saudara'])){
                                        for($i = 1 ; $i <=5 ;$i++ ){
                                            echo "Sample rekomendasi saudara. ";
                                        }
                                        echo "Rekomendasi belum terisi .<br/>";
                                    }else{
                                        echo $data['rekom']['saudara'];
                                    }
                                ?>
                            </p>
                            <b>"Guru BK"</b>
                            <p>
                                <?php
                                    if(empty($data['rekom']['gurubk'])){
                                        for($i = 1 ; $i <=5 ;$i++ ){
                                            echo "Sample rekomendasi guru BK. ";
                                        }
                                        echo "Rekomendasi belum terisi .<br/>";
                                    }else{
                                        echo $data['rekom']['gurubk'];
                                    }
                                ?>
                            </p>
                            <b>"Teman Sebaya"</b>
                            <p>
                                <?php
                                    if(empty($data['rekom']['teman'])){
                                        for($i = 1 ; $i <=5 ;$i++ ){
                                            echo "Sample rekomendasi Teman Sebaya. ";
                                        }
                                        echo "Rekomendasi belum terisi .<br/>";
                                    }else{
                                        echo $data['rekom']['teman'];
                                    }
                                ?>
                            </p>
                            <b>"Pemuka Masyarakat"</b>
                            <p>
                                <?php
                                    if(empty($data['rekom']['kolega'])){
                                        for($i = 1 ; $i <=5 ;$i++ ){
                                            echo "Sample rekomendasi pemuka masyarakat. ";
                                        }
                                        echo "Rekomendasi belum terisi .<br/>";
                                    }else{
                                        echo $data['rekom']['kolega'];
                                    }
                                ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php $this->view('gbk/bakatMinat',$data); ?>
                </div>
            </div>
        </div>
    </div>
    
<!-- </div> -->
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function(){
    $.ajax({
        url: "<?=BASEURL;?>Gbk/myPersonality/<?=$data['nisn'];?>",
        success: function(resp){
            $('#dapri').html(resp);
        }
    })
})
</script>