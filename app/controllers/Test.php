<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Test extends Controller
{
  // method default
  public function index()
  {
    $data['title']="Test Page";
    $this->view('template/header',$data);
    echo "<h1>Test Page</h1>";
    $this->view('forms/frmDHAgenda');
    $this->view('template/footer');
  }
}
