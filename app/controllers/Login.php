<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Login extends Controller
{
  // method default

  
  public function index()
  {
    $data['title'] = "Mitra Siap Karir";
    $this->view('template/header',$data);
    $this->view('login/index',$data);
    $this->view('template/footer');
  }

  public function mitra()
  {
    $data['title'] = "Mitra Siap Karir";
    $this->view('template/header',$data);
    $this->view('login/mitra');
    $this->view('template/footer');
  }

  public function siswa()
  {
    $data['title'] = "Mitra Siap Karir";
    $this->view('template/header',$data);
    $this->view('login/siswa');
    $this->view('template/footer');
  }

  public function gbk()
  {
    $data['title'] = "Mitra Siap Karir";
    $this->view('template/header',$data);
    $this->view('login/gbk');
    $this->view('template/footer');
  }

  public function mtrAuth(){
    $mitra = $this->model('Model_stakeholder')->premisi($_POST);
    // print (count($mitra));
      if( COUNT($mitra) == 3 ){
        //   idsh,nama,kategori
        $_SESSION['mitraID']    = $mitra['idsh'];
        $_SESSION['nama']       = $mitra['nama'];
        $_SESSION['kategori']   = $mitra['kategori'];
        echo "1";
      }else{
          echo "0";
      }
  }

  public function sisAuth(){
    $siswa = $this->model('Model_siswa')->premisi($_POST);
    // print (count($siswa));
      if( COUNT($siswa) == 3 ){
        //   idsh,nama,kategori
        $_SESSION['nis']    = $siswa['nis'];
        $_SESSION['nama']    = $siswa['nama'];
        $_SESSION['karir']  = $siswa['rencanaKarir'];
        echo "1";
      }else{
          echo "0";
      }
  }

  public function keluar(){
    session_destroy();
    session_unset();
    header("Location:".BASEURL);
  }
}
