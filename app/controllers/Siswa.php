<?php
class Siswa extends Controller
{
    public function __construct(){
        // print_r($_SESSION);
        if( !isset($_SESSION['nis']) ){
            header("Location:".BASEURL."Login/siswa");
        }
        // $_SESSION['nis']='0039643356';
        // $_SESSION['karir'] = 'Mondok';
    }

    public function index(){
        $data['title']  = "Siap Kerja: ".$_SESSION['nis'];
        $data['siswa'] = $this->identity();
        $data['berita'] = $this->model('Model_berita')->beritaku();
        $data['agenda'] = $this->model('Model_agenda')->agendaku();

        $this->view('template/header',$data);
        $this->view('siswa/index',$data);
        $this->view('template/footer');
    }
    
    public function info($tipe,$id){
        
        $data['title']  = 'Informasi Karir';
        $data['siswa']  = $this->identity();
        $data['tipe']   = $tipe;
        if($tipe == 'berita'){
            $data['info']=$this->model('Model_berita')->ndetil($id);
        }else{
            $data['info']=$this->model('Model_agenda')->ndetil($id);
        }

        $this->view('template/header',$data);
        $this->view('siswa/informasi',$data);
        $this->view('template/footer');

    }

    public function myPersonality($nis){
        $data['nisn']=$nis;
        $this->view('siswa/kepribadian',$data);
    }

    public function rujukan(){
        $data['title'] = 'Website Rujukan Karir';
        $data['siswa']  = $this->identity();
        $this->view('template/header',$data);
        $this->view('siswa/webRujukan',$data);
        $this->view('template/footer');
    }

    private function identity(){
        $data['siswa'] = $this->model('Model_siswa')->ndetil($_SESSION['nis']);
        return $data['siswa'];
    }


}