<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Gbk extends Controller
{
    
    public function __construct(){    
        if(!isset($_SESSION['gbk'])){     
            header("Location:".BASEURL."Login/gbk");
        }
    }

    // method default
    public function index()
    {
        $data['title'] = "Bimbingan Rencana Karir";
        $data['karir'] = $this->model('Model_siswa')->pilihanKarir();
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/index',$data);
        $this->view('template/footer');
    }

    public function siswa($pn=1){
        $data['title'] = "Siswa MAN 2";
        $data['siswa'] = $this->model('Model_siswa')->nampil($pn);
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/siswa',$data);
        $this->view('template/footer');
    }
    
    public function profil($nis){
        $data['title'] = "Proril Siswa " . $nis;
        $data['nisn'] = $nis;
        $data['siswa'] = $this->model('Model_siswa')->ndetil($nis);
        $data['nismp'] = $this->model('Model_nilaismp')->ndetil($nis);
        $data['nilai'] = $this->model('Model_nilaiMan')->cekNilai($nis);
        for ($n = 1 ; $n <= $data['nilai']['semester'] ; $n++ ){
            $data['nsmt'.$n] = $this->model('Model_nilaiMan')->MANndetil($nis.'_'.$n);
        }
        // $data['nsmt2'] = $this->model('Model_nilaiMan')->MANndetil($nis.'_2');
        // $data['nsmt3'] = $this->model('Model_nilaiMan')->MANndetil($nis.'_3');
        // $data['nsmt4'] = $this->model('Model_nilaiMan')->MANndetil($nis.'_4');
        $data['mpsmp'] = $this->model('Model_kamus')->kelompokKamus('mpsmp');
        $data['mpman'] = $this->model('Model_kamus')->kelompokKamus('mpmaa');
        $data['rekom'] = $this->model('Model_rekomendasi')->ndetil($nis);
        // $data['bakmi'] = $this->bakatMinat($nis);
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/profilSiswa',$data);
        $this->view('template/footer');
    }

    public function myPersonality($nis){
        $data['nisn']=$nis;
        $this->view('siswa/kepribadian',$data);
    }

    public function bakatMinat($data){
        $this->view('gbk/bakatMinat',$data);
    }
    
    public function siswabaru(){
        $data[title]='Input Siswa';
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/inputSiswa',$data);
        $this->view('template/footer');
    }
    
    public function nilaimasuk($nis=""){
        $data['nis'] = $nis;
        $data[title]='Input Siswa';
        if( $nis=="" ){
            $data['nilai'] =['nis'=>$nis,'abp'=>'','pkn'=>'','ind'=>'','mtk'=>'','ipa'=>'','ips'=>'','ing'=>'','sbk'=>'','pjk'=>'','aqh'=>'','aqa'=>'','fqh'=>'','ski'=>'','arb'=>''];
            $data['action']= BASEURL.'Gbk/nmtsmpbaru';
        }else{
            $data['nilai'] = $this->model('Model_nilaismp')->ndetil($nis);
            $data['action']= BASEURL.'Gbk/nmtsmplama';
        }
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/nilaimasuk',$data);
        $this->view('template/footer');
    }

    public function nmtsmpbaru(){
        if($this->model('Model_nilaismp')->nambah($_POST) > 0 ){
            Alert::setAlert("Berhasil Ditambahkan" , "Nilai Masuk MAN", 'success');
        }else{
            Alert::setAlert("Gagal Ditambahkan" , "Nilai Masuk MAN", 'danger');
        }
        header("Location:" . BASEURL ."Gbk/siswa");
    }

    public function nmtsmplama(){
        if($this->model('Model_nilaismp')->ngubah($_POST) > 0 ){
            Alert::setAlert("Berhasil dimutakhirkan" , "Nilai Masuk MAN", 'success');
        }else{
            Alert::setAlert("Gagal dimutakhirkan" , "Nilai Masuk MAN", 'danger');
        }
        header("Location:" . BASEURL ."Gbk/siswa");
    }
    
    public function nilaiSmtIpa($kodeNilai=""){
        $data['title']='Input Siswa';
        $data['kodeNilai']=$kodeNilai;
        if( $kodeNilai == "" ){
            $data['action']=BASEURL."Gbk/nilaiSmtBaru/IPA";
            $data['nilai'] = ['a_aqhd'=>'','a_aqkh'=>'','a_barb'=>'','a_bind'=>'','a_bing'=>'','a_fiqh'=>'','a_mttk'=>'','a_ppkn'=>'','a_sjid'=>'','a_skbi'=>'','b_pjok'=>'','b_pkwu'=>'','b_sbdy'=>'','ca_bio'=>'','ca_fsk'=>'','ca_kim'=>'','ca_mtk'=>''];
        }else{
            $data['action']=BASEURL."Gbk/nilaiSmtLama/IPA";
            $data['nilai'] = $this->model('Model_nilaiMan')->MANndetil($kodeNilai);
        }
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/nilaiManIpa',$data);
        $this->view('template/footer');
    }
    
    public function nilaiSmtIps(){
        $data[title]='Input Siswa';
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/nilaiManIps',$data);
        $this->view('template/footer');
    }
    
    public function nilaiSmtBhs(){
        $data[title]='Input Siswa';
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/nilaiManBhs',$data);
        $this->view('template/footer');
    }
    
    public function nilaiSmtAgm(){
        $data[title]='Input Siswa';
        
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/nilaiManAgm',$data);
        $this->view('template/footer');
    }

    public function nilaiSmtBaru($jur){
        if($jur=='IPA'){
            $op = $this->model('Model_nilaiMan')->IPAnambah($_POST);
        }elseif($jur == 'IPS'){
            $op = $this->model('Model_nilaiMan')->IPSnambah($_POST);
        }elseif($jur == 'BHS'){
            $op = $this->model('Model_nilaiMan')->BHSnambah($_POST);    
        }else{
            $op = $this->model('Model_nilaiMan')->AGMnambah($_POST);
        }

        if( $op > 0 ){
            Alert::setAlert("Berhasil ditambahkan","Data Nilai","success");
        }else{
            Alert::setAlert("Gagal ditambahkan","Data Nilai","danger");
        }

        header("Location:" . BASEURL ."Gbk/siswa");
        
    }

    public function nilaiSmtLama($jur){
        if($jur=='IPA'){
            $op = $this->model('Model_nilaiMan')->IPAngubah($_POST);
        }elseif($jur == 'IPS'){
            $op = $this->model('Model_nilaiMan')->IPSngubah($_POST);
        }elseif($jur == 'BHS'){
            $op = $this->model('Model_nilaiMan')->BHSngubah($_POST);    
        }else{
            $op = $this->model('Model_nilaiMan')->AGMngubah($_POST);
        }

        if( $op > 0 ){
            Alert::setAlert("Berhasil dimutakhirkan","Data Nilai","success");
        }else{
            Alert::setAlert("Gagal dimutakhirkan","Data Nilai","danger");
        }

        header("Location:" . BASEURL ."Gbk/siswa");
        
    }
    
    public function rekomendasi($nis=""){
        $data['title']='Input Siswa';
        $data['nis'] = $nis;
        if($nis==""){
            $data['action'] = BASEURL."Gbk/setRekom";
            $data['rekom']=['nis'=>'' , 'orangTua'=>'' , 'saudara'=>'' , 'teman'=>'' , 'gurubk'=>'' , 'kolaga'=>''];
        }else{
            $data['action']= BASEURL."Gbk/chgRekom";
            $data['rekom'] = $this->model('Model_rekomendasi')->ndetil($nis);
        }

        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/rekomendasi',$data);
        $this->view('template/footer');
    }

    public function setRekom(){
        if( $this->model('Model_rekomendasi')->nambah($_POST) > 0 ){
            Alert::setAlert("Berhasil ditambahkan" , "Data Rekomendasi ", 'success');
        }else{            
            Alert::setAlert("Gagal ditambahkan" , "Data Rekomendasi ", 'danger');
        }

        header("Location:" . BASEURL ."Gbk/siswa");
    }

    public function chgRekom(){
        if( $this->model('Model_rekomendasi')->ngubah($_POST) > 0 ){
            Alert::setAlert("Berhasil dimutakhirkan" , "Data Rekomendasi ", 'success');
        }else{            
            Alert::setAlert("Gagal dimutakhirkan" , "Data Rekomendasi ", 'danger');
        }

        header("Location:" . BASEURL ."Gbk/siswa");
    }

    public function mitra($nh=1){
        $data['title'] = "Mitra BK MAN 2";
        $data['mitra'] = $this->model('Model_stakeholder')->nampil($nh);
        $data['page'] = $nh;

        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/mitraGbk',$data);
        $this->view('template/footer');
    }

    public function mitrabaru(){
        $data['title'] = "Mitra BK MAN 2";
        $data['action'] = BASEURL . 'Gbk/setMitra';
        $data['mitra'] = ['nama'=>'','kategori'=>'','contactPerson'=>'','alamat'=>'','telepon'=>'','email'=>'','website'=>'','idsh'=>''];
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/frmMitra',$data);
        $this->view('template/footer');
    }

    public function setMitra(){
        if( $this->model('Model_stakeholder')->nambah($_POST) > 0 ){
            Alert::setAlert("Berhasil ditambahkan","Data mitra","success");
        }else{            
            Alert::setAlert("Gagal ditambahkan","Data mitra","success");
        }
        
        header("Location:" . BASEURL ."Gbk/mitra");
    }

    public function mitralama($id){
        $data['title'] = "Mitra BK MAN 2";
        $data['action'] = BASEURL . 'Gbk/chgMitra';
        $data['mitra'] = $this->model('Model_stakeholder')->ndetil($id);
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/frmMitra',$data);
        $this->view('template/footer');
    }

    public function chgMitra(){
        if( $this->model('Model_stakeholder')->ngubah($_POST) > 0 ){
            Alert::setAlert("Berhasil dimutakhirkan","Data mitra","success");
        }else{            
            Alert::setAlert("Gagal dimutakhirkan","Data mitra","success");
        }
        
        header("Location:" . BASEURL ."Gbk/mitra");
    }

    public function rmvMitra(){
        if( $this->model('Model_stakeholder')->ndupak($_POST['idsh']) > 0 ) echo "1";
    }

    public function agenda($nh=1){
        $data['title'] = "Agenda Bina Karir";
        $data['acara'] = $this->model('Model_agenda')->nampil($nh);
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/agenda',$data);
        $this->view('template/footer');
    }

    public function agendabaru($idsh){
        $data['title'] = "Agenda Bina Karir";
        $data['action'] = BASEURL . 'Gbk/setAgenda';
        $data['acara'] = ['promotor'=>'' , 'namaAgenda'=>'' , 'waktu'=>'' , 'tempat'=>'' , 'deskripsi'=>'' , 'idAgenda
        '=>''];
        $data['idsh'] = $idsh;
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/frmAgenda',$data);
        $this->view('template/footer');
    }

    public function promotorlist($nama){
        $data = $this->model('Model_stakeholder')->shByName($nama);
        echo json_encode($data,JSON_PRETTY_PRINT);
    }

    public function setAgenda(){
        if($this->model('Model_agenda')->nambah($_POST) > 0 ){
            Alert::setAlert('Berhasil ditambahkan','data agenda','success');
        }else{
            Alert::setAlert('Gagal ditambahkan','data agenda','warning');
        }
        header("Location:". BASEURL ."Gbk/agenda");
    }

    public function dagenda($id){
        $data['title'] = 'Agenda #'.$id;
        $data['idsh'] = $id;
        $data['acara'] = $this->model('Model_agenda')->ndetil($id);
        $this->view('template/header',$data);
        $this->view('gbk/navbar');
        $this->view('gbk/agendetil',$data);
        $this->view('template/footer');
    }
}
