<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Mitra extends Controller
{
  // method default

  public function __construct(){
    if(!isset($_SESSION['mitraID']))
        header("Location:" . BASEURL ."Login/mitra"); 
  }

  public function index()
  {
    $data['title'] = "Mitra Siap Karir";
    $data['identity'] = $this->identity();
    $data['berita'] = $this->model('Model_berita')->Mitradetil($_SESSION['mitraID']);
    $this->view('template/header',$data);
    $this->view('mitra/index',$data);
    $this->view('template/footer');
  }

  public function login()
  {
    $data['title'] = "Mitra Siap Karir";
    $this->view('template/header',$data);
    $this->view('mitra/login');
    $this->view('template/footer');
  }

  private function identity(){
    $data['identity'] = $this->model('Model_stakeholder')->ndetil($_SESSION['mitraID']);
    return $data['identity'];
    // $this->view('mitra/identity$data',$data);
  }

  public function berita($id){
    if( $id == 0){
      $this->beritaBaru();
    }else{
      $this->beritaLama($id);
    }
  }

  private function beritaBaru(){
    $data['title'] = "Berita Baru";
    $data['berita'] = [
      'idBerita '=>'',' rilis '=>'',' judul'=>'',' konten'=>'',' tags' => ''
    ];
    $data['action'] = BASEURL.'Mitra/setBerita';
    $data['identity'] = $this->identity();

    $this->view('template/header',$data);
    $this->view('mitra/frmBerita',$data);
    $this->view('template/footer');
  }

  private function beritaLama($id){
    $data['title'] = "Ganti Berita";
    $data['berita'] = $this->model('Model_berita')->ndetil($id);
    $data['action'] = BASEURL.'Mitra/chgBerita';
    $data['identity'] = $this->identity();

    $this->view('template/header',$data);
    $this->view('mitra/frmBerita',$data);
    $this->view('template/footer');
  }

  public function setBerita(){
    if ( $this->model('Model_berita')->nambah($_POST) > 0 ){
      Alert::setAlert('Berhasil ditambahkan','Berita','success');
    }else{
      Alert::setAlert('Gagal ditambahkan','Berita','warning');
    }
    header("Location:" . BASEURL ."Mitra");
  }
  
  public function chgBerita(){
    if ( $this->model('Model_berita')->ngubah($_POST) > 0 ){
      Alert::setAlert('Berhasil dimutakhirkan','Berita','success');
    }else{
      Alert::setAlert('Gagal dimutakhirkan','Berita','warning');
    }
    header("Location:" . BASEURL ."Mitra");
  }

  public function beritane($id){
    $data['title'] = 'Detil Berita #'.$id;
    $data['identity'] = $this->identity();
    $data['berita'] = $this->model('Model_berita')->ndetil($id);
    $this->view('template/header',$data);
    $this->view('mitra/berita',$data);
    $this->view('template/footer');
  }
}
