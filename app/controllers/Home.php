<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/index',$data);
    $this->view('template/footer');
  }

  public function authenticated(){
    // $passkey = md5('damhawqidis');
    $passkey = 'b4c93b2ffaf2a50c59669788bea500a6';
    if( md5($_POST['passkey']) == $passkey ){
      $_SESSION['gbk'] = 'Sidik Wibowo Ahmad';
      echo "1";
    }else{
      echo "0";
    }
  }

  public function kuliah(){
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/kuliah');
    $this->view('template/footer');
  }

  public function kerja(){
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/kerja');
    $this->view('template/footer');
  }

  public function kursus(){
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/kursus');
    $this->view('template/footer');
  }

  public function wirausaha(){
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/wirausaha');
    $this->view('template/footer');
  }

  public function pesantren(){
    $data['title'] = "Bimbingan Rencana Karir";
    $this->view('template/header',$data);
    $this->view('template/navbar');
    $this->view('home/pesantren');
    $this->view('template/footer');
  }
}
