<?php
class Model_berita
{
    private $table = "berita";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Fungsi CRUD
    public function nambah($data){
        $sql = "INSERT INTO " . $this->table . " SET rilis=:rilis,author=:author,judul=:judul,konten=:konten,tags=:tags";
        $this->db->query($sql);
        $this->db->bind('rilis',$data['rilis']);
        $this->db->bind('author',$data['author']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('konten',$data['konten']);
        $this->db->bind('tags',$data['tags']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET rilis=:rilis,author=:author,judul=:judul,konten=:konten,tags=:tags WHERE idBerita=:idBerita";
        $this->db->query($sql);
        $this->db->bind('rilis',$data['rilis']);
        $this->db->bind('author',$data['author']);
        $this->db->bind('judul',$data['judul']);
        $this->db->bind('konten',$data['konten']);
        $this->db->bind('tags',$data['tags']);
        $this->db->bind('idBerita',$data['idBerita']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nampil($kat){
        
        $sql = "SELECT berita.* , stakeHolder.kategori FROM berita , stakeHolder WHERE stakeHolder.idhs = berita.author ORDER BY rilis DESC LIMIT 20";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function ndetil($id){
        $sql = "SELECT berita.*, stakeHolder.nama FROM berita, stakeHolder WHERE berita.idBerita=:id && stakeHolder.idsh=berita.author";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    public function Mitradetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE author=:id ORDER BY rilis DESC LIMIT " . rows ;
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultSet();
    }
    
    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE idBerita=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->rowCount();
    }

    public function beritaku(){
        $sql = "SELECT b.idBerita , b.rilis, s.nama author , b.judul , b.tags FROM berita b, stakeHolder s WHERE s.kategori = :karir && b.author = s.idsh";
        $this->db->query($sql);
        $this->db->bind('karir',$_SESSION['karir']);
        return $this->db->resultSet();
    }
    
}

/*
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        return $this->db->resultOne();
        return $this->db->resultSet();
        $this->db->execute();
        return $this->db->rowCount();

*/