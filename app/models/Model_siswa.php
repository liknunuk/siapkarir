<?php
class Model_siswa
{
    private $table = "siswa";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Fungsi CRUD
    public function nambah($data){
        $sql = "INSERT INTO " . $this->table . " SET nama=:nama , jnsKelamin=:jnsKelamin , tempatLahir=:tempatLahir , tanggalLahir=:tanggalLahir , jurusan=:jurusan , rencanaKarir=:rencanaKarir , kepribadian=:kepribadian , nis=:nis";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('jnsKelamin',$data['jnsKelamin']);
        $this->db->bind('tempatLahir',$data['tempatLahir']);
        $this->db->bind('tanggalLahir',$data['tanggalLahir']);
        $this->db->bind('jurusan',$data['jurusan']);
        $this->db->bind('rencanaKarir',$data['rencanaKarir']);
        $this->db->bind('kepribadian',$data['kepribadian']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET nama=:nama , jnsKelamin=:jnsKelamin , tempatLahir=:tempatLahir , tanggalLahir=:tanggalLahir , jurusan=:jurusan , rencanaKarir=:rencanaKarir , kepribadian=:kepribadian WHERE nis=:nis";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('jnsKelamin',$data['jnsKelamin']);
        $this->db->bind('tempatLahir',$data['tempatLahir']);
        $this->db->bind('tanggalLahir',$data['tanggalLahir']);
        $this->db->bind('jurusan',$data['jurusan']);
        $this->db->bind('rencanaKarir',$data['rencanaKarir']);
        $this->db->bind('kepribadian',$data['kepribadian']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nampil( $nh=1 ){
        $row = ( $nh - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY nama LIMIT " . $row  . "," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function ndetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // Trx Queries
    public function pilihanKarir(){
        $sql = "SELECT rencanaKarir karir , COUNT( rencanaKarir ) jumlah FROM siswa GROUP BY rencanaKarir";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // Login
    public function premisi($data){
        $sql = "SELECT nis,nama,rencanaKarir FROM siswa WHERE nis=:nis";
        $this->db->query($sql);
        $this->db->bind('nis',$data['nis']);
        return $this->db->resultOne();
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/