<?php
class Model_agenda
{
    private $table = "agenda";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Fungsi CRUD
    public function nambah($data){
        $sql = "INSERT INTO " . $this->table . " SET promotor=:promotor,namaAgenda=:namaAgenda,waktu=:waktu,tempat=:tempat,deskripsi=:deskripsi";
        $this->db->query($sql);        
        $this->db->bind('promotor',$data['promotor']);
        $this->db->bind('namaAgenda',$data['namaAgenda']);
        $this->db->bind('waktu',$data['tanggal'].' '.$data['waktu']);
        $this->db->bind('tempat',$data['tempat']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET promotor=:promotor,namaAgenda=:namaAgenda,waktu=:waktu,tempat=:tempat,deskripsi=:deskripsi WHERE idAgenda=:idAgenda";
        $this->db->query($sql);
        $this->db->bind('promotor',$data['promotor']);
        $this->db->bind('namaAgenda',$data['namaAgenda']);
        $this->db->bind('waktu',$data['waktu']);
        $this->db->bind('tempat',$data['tempat']);
        $this->db->bind('deskripsi',$data['deskripsi']);
        $this->db->bind('idAgenda',$data['idAgenda']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nampil($nh = 1){
        $row = ( $nh - 1 ) * rows;
        $sql = "SELECT agenda.* , stakeHolder.kategori, stakeHolder.nama promotor FROM agenda , stakeHolder WHERE stakeHolder.idsh = agenda.promotor ORDER BY waktu DESC LIMIT $row,".rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function ndetil($id){
        $sql = "SELECT agenda.* , stakeHolder.nama namaPromotor FROM agenda , stakeHolder WHERE idAgenda=:id && stakeHolder.idsh = agenda.promotor";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }
    
    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE idAgenda=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // Kehadiran Siswa dalam Agenda
    public function presensi($id,$nis){
        $presensi = $this->siswaHadir($id);
        $siswa = explode(",",$presensi['hadir']);
        sort($siswa);
        if( in_array($nis)){
            return "0";
        }else{
            $sql = "UPDATE " . $this->table ." SET siswaHadir=:siswa WHERE idAgenda=:id";
            $this->db->query($sql);
            $this->db->bind('id',$id);
            $this->db->execute();
            return $this->db->rowCount;
        }
    }

    private function siswaHadir($id){
        $sql = "SELECT siswaHadir hadir FROM " . $this->table . " WHERE idAgenda=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }

    // Agenda mitra
    public function agendamitra($kat){
        $sql = "SELECT agenda.* , stakeHolder.kategori FROM agenda , stakeHolder WHERE kategori=:kat && stakeHolder.idsh = agenda.promotor ORDER BY waktu DESC LIMIT 20";
        $this->db->query($sql);
        $this->db->bind('kat',$kat);
        return $this->db->resultSet();
    }
    
    public function agendaku(){
        $sql = "SELECT a.idAgenda , s.nama promotor , a.namaAgenda , a.waktu , a.tempat , a.deskripsi FROM agenda a , stakeHolder s WHERE s.kategori=:karir && s.idsh = a.promotor";
        $this->db->query($sql);
        $this->db->bind('karir',$_SESSION['karir']);
        return $this->db->resultSet();
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/