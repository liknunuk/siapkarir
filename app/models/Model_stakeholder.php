<?php
class Model_stakeholder
{
    private $table = "stakeHolder";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
   // Fungsi CRUD
    public function nambah($data){
        $sql = "INSERT INTO " . $this->table . " SET nama=:nama,kategori=:kategori,contactPerson=:contactPerson,alamat=:alamat,telepon=:telepon,email=:email,website=:website";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('kategori',$data['rencanaKarir']);
        $this->db->bind('contactPerson',$data['contactPerson']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('email',$data['email']);
        $this->db->bind('website',$data['website']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET nama=:nama,kategori=:kategori,contactPerson=:contactPerson,alamat=:alamat,telepon=:telepon,email=:email,website=:website WHERE idsh=:idsh";
        $this->db->query($sql);
        $this->db->bind('nama',$data['nama']);
        $this->db->bind('kategori',$data['rencanaKarir']);
        $this->db->bind('contactPerson',$data['contactPerson']);
        $this->db->bind('alamat',$data['alamat']);
        $this->db->bind('telepon',$data['telepon']);
        $this->db->bind('email',$data['email']);
        $this->db->bind('website',$data['website']);
        $this->db->bind('idsh',$data['idsh']);
        $this->db->execute();
        return $this->db->rowCount();
   }

    public function nampil($nh=1){
        $row = ( $nh - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY nama LIMIT $row," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }
    
    public function ndetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE idsh=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }
    
    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE idsh=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }
    
    // Filtered Query
    
    public function shByReka($kategori,$nh=1){
        $row = ( $nh - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " WHERE kategori=:kategori ORDER BY nama LIMIT $row," . rows;
        $this->db->query($sql);
        $this->db->bind('kategori',$kategori);
        return $this->db->resultSet();
    }

    public function shByName($nama){
        $sql = "SELECT idsh, nama FROM " . $this->table . " WHERE nama LIKE :nama ORDER BY nama LIMIT 20";
        $this->db->query($sql);
        $this->db->bind('nama','%'.$nama.'%');
        return $this->db->resultSet();
    }

    public function premisi($data){
        $sql = "SELECT idsh,nama,kategori FROM stakeHolder WHERE telepon = :telepon LIMIT 1";
        $this->db->query($sql);
        $this->db->bind('telepon',$data['telepon']);
        return $this->db->resultOne();
    }



}

/*
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/