<?php
class Model_rekomendasi
{
    private $table = "rekomendasi";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
   // Fungsi CRUD
    public function nambah($data){
        $sql = "INSERT INTO ". $this->table . " SET orangTua=:orangTua,teman=:teman,saudara=:saudara,kolega=:kolega,gurubk=:gurubk,nis=:nis";
        $this->db->query($sql);
        $this->db->bind('orangTua',$data['orangTua']);
        $this->db->bind('teman',$data['teman']);
        $this->db->bind('saudara',$data['saudara']);
        $this->db->bind('kolega',$data['kolega']);
        $this->db->bind('gurubk',$data['gurubk']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubah($data){
        $sql = "UPDATE ". $this->table . " SET orangTua=:orangTua,teman=:teman,saudara=:saudara,kolega=:kolega,gurubk=:gurubk WHERE nis=:nis";
        $this->db->query($sql);
        $this->db->bind('orangTua',$data['orangTua']);
        $this->db->bind('teman',$data['teman']);
        $this->db->bind('saudara',$data['saudara']);
        $this->db->bind('kolega',$data['kolega']);
        $this->db->bind('gurubk',$data['gurubk']);
        $this->db->bind('nis',$data['nis']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nampil($nh=1){}

    public function ndetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
    }
    
    public function ndupak($id){
        $sql = "DELETE " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/