<?php
class Model_nilaismp
{
    private $table = "nilaiSMP";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
   // Fungsi CRUD
   public function nambah($data){
        $sql = "INSERT INTO " . $this->table . " SET abp=:abp,pkn=:pkn,ind=:ind,mtk=:mtk,ipa=:ipa,ips=:ips,ing=:ing,sbk=:sbk,pjk=:pjk,mlk=:mlk,aqh=:aqh,aqa=:aqa,fqh=:fqh,ski=:ski,arb=:arb,nis=:nis";

        $this->db->query($sql);
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('abp',$data['abp']);
        $this->db->bind('pkn',$data['pkn']);
        $this->db->bind('ind',$data['ind']);
        $this->db->bind('mtk',$data['mtk']);
        $this->db->bind('ipa',$data['ipa']);
        $this->db->bind('ips',$data['ips']);
        $this->db->bind('ing',$data['ing']);
        $this->db->bind('sbk',$data['sbk']);
        $this->db->bind('pjk',$data['pjk']);
        $this->db->bind('mlk',$data['mlk']);
        $this->db->bind('aqh',$data['aqh']);
        $this->db->bind('aqa',$data['aqa']);
        $this->db->bind('fqh',$data['fqh']);
        $this->db->bind('ski',$data['ski']);
        $this->db->bind('arb',$data['arb']);
        $this->db->execute();
        return $this->db->rowCount();
   }

   public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET abp=:abp,pkn=:pkn,ind=:ind,mtk=:mtk,ipa=:ipa,ips=:ips,ing=:ing,sbk=:sbk,pjk=:pjk,mlk=:mlk,aqh=:aqh,aqa=:aqa,fqh=:fqh,ski=:ski,arb=:arb WHERE nis=:nis";

        $this->db->query($sql);
        $this->db->bind('nis',$data['nis']);
        $this->db->bind('abp',$data['abp']);
        $this->db->bind('pkn',$data['pkn']);
        $this->db->bind('ind',$data['ind']);
        $this->db->bind('mtk',$data['mtk']);
        $this->db->bind('ipa',$data['ipa']);
        $this->db->bind('ips',$data['ips']);
        $this->db->bind('ing',$data['ing']);
        $this->db->bind('sbk',$data['sbk']);
        $this->db->bind('pjk',$data['pjk']);
        $this->db->bind('mlk',$data['mlk']);
        $this->db->bind('aqh',$data['aqh']);
        $this->db->bind('aqa',$data['aqa']);
        $this->db->bind('fqh',$data['fqh']);
        $this->db->bind('ski',$data['ski']);
        $this->db->bind('arb',$data['arb']);
        $this->db->execute();
        return $this->db->rowCount();
   }
     
   public function nampil($data){}

   public function ndetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
    }
    
    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE nis=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        return $this->db->resultOne();
   }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/