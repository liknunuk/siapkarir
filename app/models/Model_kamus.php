<?php
class Model_kamus
{
    private $table = "kamus";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Fungsi CRUD
    public function nambah($data){
        $sql = "UPDATE " . $this->table . " SET klp=:klp,kode=:kode,arti=:arti";
        $this->db->query($sql);
        $this->db->bind('klp',$data['klp']);
        $this->db->bind('kode',$data['kode']);
        $this->db->bind('arti',$data['arti']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubah($data){
        $sql = "UPDATE " . $this->table . " SET klp=:klp,kode=:kode,arti=:arti WHERE idx=:idx";
        $this->db->query($sql);
        $this->db->bind('klp',$data['klp']);
        $this->db->bind('kode',$data['kode']);
        $this->db->bind('arti',$data['arti']);
        $this->db->bind('idx',$data['idx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nampil($nh=1){
        $row = ( $nh - 1 ) * rows;
        $sql = "SELECT * FROM " . $this->table . " ORDER BY klp,kode LIMIT $row," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function ndetil($id){}
    
    public function ndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE idx=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function kelompokKamus($klp){
        $sql = " SELECT * FROM  " .$this->table . " WHERE klp=:klp ORDER BY kode";
        $this->db->query($sql);
        $this->db->bind('klp',$klp);
        return $this->db->resultSet();
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/