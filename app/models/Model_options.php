<?php
class Model_options
{
    public function jurusan(){
        $opsi = '
        <select name="jurusan" id="sisJurusan" class="form-control">
            <option value="">Pilih Jurusan</option>
            <option value="Agama">Agama</option>
            <option value="Bahasa">Bahasa</option>
            <option value="IPA">IPA</option>
            <option value="IPS">IPS</option>
        </select>
        ';
        return $opsi;
    }

    public function kelamin(){
        $opsi = '
        <select name="jnsKelamin" id="jnKelamin" class="form-control">
            <option value="Laki-laki">Laki-Laki</option>
            <option value="Perempuan">Perempuan</option>
        </select>
        ';
        return $opsi;
    }

    public function karir(){
        $opsi ='
        <select name="rencanaKarir" id="renka" class="form-control">
            <option value="Kuliah">Kuliah</option>
            <option value="Kerja">Kerja</option>
            <option value="Kursus">Kursus</option>
            <option value="Wirausaha">Wirausaha</option>
            <option value="Mondok">Mondok</option>
        </select>
        ';
        return $opsi;
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/