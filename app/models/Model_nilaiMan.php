<?php
class Model_nilaiMan
{
    private $table = "nilaiMan";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
   // Fungsi CRUD IPA
    public function IPAnambah($data){
        $sql = "INSERT INTO " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,ca_bio=:ca_bio,ca_fsk=:ca_fsk,ca_kim=:ca_kim,ca_mtk=:ca_mtk,kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('ca_bio',$data['ca_bio']);
        $this->db->bind('ca_fsk',$data['ca_fsk']);
        $this->db->bind('ca_kim',$data['ca_kim']);
        $this->db->bind('ca_mtk',$data['ca_mtk']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function IPAngubah($data){
        $sql = "UPDATE " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,ca_bio=:ca_bio,ca_fsk=:ca_fsk,ca_kim=:ca_kim,ca_mtk=:ca_mtk WHERE kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('ca_bio',$data['ca_bio']);
        $this->db->bind('ca_fsk',$data['ca_fsk']);
        $this->db->bind('ca_kim',$data['ca_kim']);
        $this->db->bind('ca_mtk',$data['ca_mtk']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function IPSnambah($data){
        $sql = "INSERT INTO " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cs_eko=:cs_eko,cs_geo=:cs_geo,cs_sej=:cs_sej,cs_sos=:cs_sos,kodeNilai=:kodeNilai ";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cs_eko',$data['cs_eko']);
        $this->db->bind('cs_geo',$data['cs_geo']);
        $this->db->bind('cs_sej',$data['cs_sej']);
        $this->db->bind('cs_sos',$data['cs_sos']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function IPSngubah($data){
        $sql = "UPDATE " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cs_eko=:cs_eko,cs_geo=:cs_geo,cs_sej=:cs_sej,cs_sos=:cs_sos WHERE kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cs_eko',$data['cs_eko']);
        $this->db->bind('cs_geo',$data['cs_geo']);
        $this->db->bind('cs_sej',$data['cs_sej']);
        $this->db->bind('cs_sos',$data['cs_sos']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function BHSnambah($data){
        $sql = "INSERT INTO " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cb_atp=:cb_atp,cb_sal=:cb_sal,cb_sid=:cb_sid,cb_sig=:cb_sig,kodeNilai=:kodeNilai ";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cb_atp',$data['cb_atp']);
        $this->db->bind('cb_sal',$data['cb_sal']);
        $this->db->bind('cb_sid',$data['cb_sid']);
        $this->db->bind('cb_sig',$data['cb_sig']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function BHSngubah($data){
        $sql = "UPDATE " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cb_atp=:cb_atp,cb_sal=:cb_sal,cb_sid=:cb_sid,cb_sig=:cb_sig WHERE kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cb_atp',$data['cb_atp']);
        $this->db->bind('cb_sal',$data['cb_sal']);
        $this->db->bind('cb_sid',$data['cb_sid']);
        $this->db->bind('cb_sig',$data['cb_sig']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function AGMnambah($data){
        $sql = "INSERT INTO " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cg_akh=:cg_akh,cg_arb=:cg_arb,cg_fqh=:cg_fqh,cg_hds=:cg_hds,cg_klm=:cg_klm,cg_taf=:cg_taf,kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cg_akh',$data['cg_akh']);
        $this->db->bind('cg_arb',$data['cg_arb']);
        $this->db->bind('cg_fqh',$data['cg_fqh']);
        $this->db->bind('cg_hds',$data['cg_hds']);
        $this->db->bind('cg_klm',$data['cg_klm']);
        $this->db->bind('cg_taf',$data['cg_taf']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function AGMngubah($data){
        $sql = "UPDATE " . $this->table . " SET a_aqhd=:a_aqhd,a_aqkh=:a_aqkh,a_barb=:a_barb,a_bind=:a_bind,a_bing=:a_bing,a_fiqh=:a_fiqh,a_mttk=:a_mttk,a_ppkn=:a_ppkn,a_sjid=:a_sjid,a_skbi=:a_skbi,b_pjok=:b_pjok,b_pkwu=:b_pkwu,b_sbdy=:b_sbdy,cg_akh=:cg_akh,cg_arb=:cg_arb,cg_fqh=:cg_fqh,cg_hds=:cg_hds,cg_klm=:cg_klm,cg_taf=:cg_taf WHERE kodeNilai=:kodeNilai";
        $this->db->query($sql);
        $this->db->bind('a_aqhd',$data['a_aqhd']);
        $this->db->bind('a_aqkh',$data['a_aqkh']);
        $this->db->bind('a_barb',$data['a_barb']);
        $this->db->bind('a_bind',$data['a_bind']);
        $this->db->bind('a_bing',$data['a_bing']);
        $this->db->bind('a_fiqh',$data['a_fiqh']);
        $this->db->bind('a_mttk',$data['a_mttk']);
        $this->db->bind('a_ppkn',$data['a_ppkn']);
        $this->db->bind('a_sjid',$data['a_sjid']);
        $this->db->bind('a_skbi',$data['a_skbi']);
        $this->db->bind('b_pjok',$data['b_pjok']);
        $this->db->bind('b_pkwu',$data['b_pkwu']);
        $this->db->bind('b_sbdy',$data['b_sbdy']);
        $this->db->bind('cg_akh',$data['cg_akh']);
        $this->db->bind('cg_arb',$data['cg_arb']);
        $this->db->bind('cg_fqh',$data['cg_fqh']);
        $this->db->bind('cg_hds',$data['cg_hds']);
        $this->db->bind('cg_klm',$data['cg_klm']);
        $this->db->bind('cg_taf',$data['cg_taf']);
        $this->db->bind('kodeNilai',$data['kodeNilai']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function MANnampil($data){}

    public function MANndetil($id){
        $sql = "SELECT * FROM " . $this->table . " WHERE kodeNilai=:id ORDER BY kodeNilai";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->resultOne();
    }

    public function MANndupak($id){
        $sql = "DELETE FROM " . $this->table . " WHERE kodeNilai=:id";
        $this->db->query($sql);
        $this->db->bind('id',$id);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // add ons functions
    public function cekNilai($nis){
        $sql = "SELECT COUNT(kodeNilai) semester FROM nilaiMan WHERE kodeNilai LIKE :nis ";

        $this->db->query($sql);
        $this->db->bind('nis',$nis.'%');
        return $this->db->resultOne();
    }

}

/*
    
        $sql = "";
        $this->db->query($sql);
        $this->db->bind('xxx',$data['xxx']);
        $this->db->execute();
        return $this->db->resultOne();
        return $this->db->resultSet();
        return $this->db->rowCount();

*/

/*
mp_ipa
a_aqhd,a_aqkh,a_barb,a_bind,a_bing,a_fiqh,a_mttk,a_ppkn,a_sjid,a_skbi,b_pjok,b_pkwu,b_sbdy,ca_bio,ca_fsk,ca_kim,ca_mtk,kodeNilai

mp_ips
a_aqhd,a_aqkh,a_barb,a_bind,a_bing,a_fiqh,a_mttk,a_ppkn,a_sjid,a_skbi,b_pjok,b_pkwu,b_sbdy,cs_eko,cs_geo,cs_sej,cs_sos,kodeNilai

mp_bhs
a_aqhd,a_aqkh,a_barb,a_bind,a_bing,a_fiqh,a_mttk,a_ppkn,a_sjid,a_skbi,b_pjok,b_pkwu,b_sbdy,cb_atp,cb_sal,cb_sid,cb_sig,kodeNilai

mp_agm
a_aqhd,a_aqkh,a_barb,a_bind,a_bing,a_fiqh,a_mttk,a_ppkn,a_sjid,a_skbi,b_pjok,b_pkwu,b_sbdy,cg_akh,cg_arb,cg_fqh,cg_hds,cg_klm,cg_taf,kodeNilai

*/